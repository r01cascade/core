# CASCADE core

This repsoitory contains the `core` of CASCADE Repast HPC models.

It contains common functionality for a number of CASCADE models, and does produce a specific model iteself.

There are some tests in the `unitTests` subdirectory.

## Dependencies

+ An installation of Repast HPC >= 2.2.0, including Boost
+ An MPI compiler, such as MPICH, with the `mpicxx` binary.
+ `make`


## Compilation

Compilation requires the dependencies listed above.

There are options for informing the compiler of the locations of RepastHPC, Boost and `MPICXX`:

+ Using Environment Variables `PATH`, `CPLUS_INCLUDE_PATH`, `LIBRARY_PATH`, `LD_LIBRARY_PATH`
+ Creation of an `env` file from the included reference `env.default`

If you intend to use Environment variables you should:

+ Ensure the directory containing `mpicxx` is on your `PATH`
+ Add the Repast HPC `include` directory to `CPLUS_INCLUDE_PATH`
+ Add the Repast HPC `lib` directory to `LIBRARY_PATH` and `LD_LIBRARY_PATH`
+ Add the Boost `include` directory to `CPLUS_INCLUDE_PATH`
+ Add the Boost `lib` directory to `LIBRARY_PATH` and `LD_LIBRARY_PATH`
+ Set an environmental variable `REPASTVERSION` to a value such as `2.2.0`, subject to your repast installation

If you intend to use the `env` file:

+ Copy the default env file to `env`: `cp env.default env`
+ Adjust the included variables to correclty locate your `mpicxx`, Boost and RepastHPC installations

Alternatively, [Singularity/aptainer]() containers can be used to provide all required depenedenices. For more information see the [singularity-containers](https://bitbucket.org/r01cascade/singularity-containers/) repository.

The Makefile in the root of this repository does not produce an executable due to the absence of a `main` method. The root makefile compiles each source file to object files but does not link a binary.  

```bash
make all -j `nproc`
```

This makefile uses incremental compilation by default to reduce compile time by avoiding unneccesary work.

If the Makefile itself has been changed, or your environment has changed (repast version, mpi compiler etc) you should clean the build environment prior to compilation.

```bash
make clean
make all -j `nproc`
```

### Compiler Build Configuration

The makefile includes several lines which can be enabled/disabled to change the compiler flags related to optimistaion and debug symbols. 

By default, level 3 optimistaions are set (`-O3`), with no debug information. 

See the makefile for more information.

<!-- 
## Unit Tests

@todo - document unit test compilation / usage.

This repository contains some unit tests for the `core`, located in the `unitTests` directory.

### Compiling the unit tests

The unit tetss require the same dependencies as above. 


### Executing the unit tests
 -->
