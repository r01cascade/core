#ifndef INCLUDE_GLOBALS_H_
#define INCLUDE_GLOBALS_H_
#include <map>
#include <utility>
#include <vector>
#include <unordered_map>
#include <string>

extern bool FAIL_FAST;
extern bool SOCIAL_NETWORK;

// Values known at compile time can be marked constexpr in the header, this provides the compiler with additional optimisation opportunity.
constexpr bool MALE = true;
constexpr bool FEMALE = false;
constexpr int NUM_SEX = 2;
constexpr int NUM_AGE_GROUPS = 9;
constexpr int MIN_AGE = 12;
constexpr int MAX_AGE = 100;
constexpr int MAX_DRINKS = 30;
constexpr int AGE_GROUPS[NUM_AGE_GROUPS] = { 14, 24, 34, 44, 54, 64, 74, 84, MAX_AGE };
constexpr int AGE_RANGE = 80 - MIN_AGE;

constexpr int MAX_DRINK_LEVEL = 7;
constexpr int MIN_DRINK_LEVEL = 1;
constexpr int DRINK_RANGE = MAX_DRINK_LEVEL - MIN_DRINK_LEVEL;

constexpr int MAX_INCOME = 1000000;

extern std::map<int, std::pair<double, double> > MEAN_SD_LOOKUP_TABLE;
extern std::unordered_map<std::string, double> deathRateTable;//storing death rate by characteristics
extern std::unordered_map<std::string, double> migrationOutTable;//storing migration-out rate by characteristics

/* NEW ACTION MECH */
// extern const int NUM_SCHEMA;
constexpr int NUM_SCHEMA = 5;
extern double BETA_ATTITUDE;
extern double BETA_NORM;
extern double BETA_PBC;
extern int HABIT_STOCK_DAYS;
//extern int HABIT_UPDATE_INTERVAL;

extern double AGE_SIMILARITY_BETA;
extern double SEX_SIMILARITY_BETA;
extern double RACE_SIMILARITY_BETA;
extern double IS_DRINKING_TODAY_SIMILARITY_BETA;
extern double NUMBER_DRINKS_TODAY_SIMILARITY_BETA;
extern double IS_12_MONTH_DRINKERS_SIMILARITY_BETA;
extern double SELECTION_FREQUENCY_BETA;
extern double TOTAL_DRINKS_PER_ANNUM_SIMILARITY_BETA;
extern double MARITAL_STATUS_SIMILARITY_BETA;
extern double PARENTHOOD_STATUS_SIMILARITY_BETA;
extern double EMPLOYMENT_STATUS_SIMILARITY_BETA;
extern double MEAN_DRINKS_TODAY_SIMILARITY_BETA;
extern double SD_DRINKS_TODAY_SIMILARITY_BETA;
extern double INCOME_SIMILARITY_BETA;
extern double PAST_YEAR_N_SIMILARITY_BETA;
extern double SELECTION_QUANTITY_BETA;
extern double OUTDEGREE_BETA;
extern double RECIPROCITY_BETA;
extern double PREFERENTIAL_ATTACHMENT_BETA;
extern double TRANSITIVE_TRIPLES_BETA;
extern double GPD_30_DAYS_BETA;

extern bool AGE_SIMILARITY_FLAG;
extern bool SEX_SIMILARITY_FLAG;
extern bool RACE_SIMILARITY_FLAG;
extern bool IS_DRINKING_TODAY_SIMILARITY_FLAG;
extern bool NUMBER_DRINKS_TODAY_SIMILARITY_FLAG;
extern bool IS_12_MONTH_DRINKERS_SIMILARITY_FLAG;
extern bool SELECTION_FREQUENCY_FLAG;
extern bool TOTAL_DRINKS_PER_ANNUM_SIMILARITY_FLAG;
extern bool MARITAL_STATUS_SIMILARITY_FLAG;
extern bool PARENTHOOD_STATUS_SIMILARITY_FLAG;
extern bool EMPLOYMENT_STATUS_SIMILARITY_FLAG;
extern bool MEAN_DRINKS_TODAY_SIMILARITY_FLAG;
extern bool SD_DRINKS_TODAY_SIMILARITY_FLAG;
extern bool INCOME_SIMILARITY_FLAG;
extern bool PAST_YEAR_N_SIMILARITY_FLAG;
extern bool SELECTION_QUANTITY_FLAG;
extern bool OUTDEGREE_FLAG;
extern bool RECIPROCITY_FLAG;
extern bool PREFERENTIAL_ATTACHMENT_FLAG;
extern bool TRANSITIVE_TRIPLES_FLAG;
extern bool GPD_30_DAYS_FLAG;

extern bool AGENT_LEVEL_OUTPUT;
//TODO: Move theory-specific global variables out of core

/**** RATIONAL CHOICE ****/
extern double UNIT_PRICE[40];
extern double TEMP_LEGAL_RISK;
extern double DECAY_BASE_RATE;
extern int DAYS_TO_DEVELOP_WITHDRAWAL;
extern int WITHDRAWAL_WASHOUT_DIVISOR; 
extern double HOURS_FREE_TIME_MEAN; 
extern double HOURS_FREE_TIME_SD;
extern int TOTAL_YEARS_CONSUMPTION_STOCK; 
extern int HEAVY_DRINKS_PER_DAY;
/**** RATIONAL CHOICE ****/


/**** CONTAGION ****/
constexpr int POTENTIAL_BUDDIES_SIZE = 0;
extern  double FREQUENCY_INFLUENCE_BETA;
extern  double QUANTITY_INFLUENCE_BETA;
extern  double INFLUENCE_LAMBDA;
extern  double SELECTION_LAMBDA;

/**** CONTAGION ****/

/**** ROLES ****/
constexpr int THRESHOLD_QUANTITY_HEAVEY_MALE_DRINKER = 30;//gram/day
constexpr int THRESHOLD_QUANTITY_HEAVEY_FEMALE_DRINKER = 20;//gram/day
constexpr int IDFOROUTPUT = 260;
extern bool ROLES_SOCIALISATION_ON;
/**** ROLES ****/

#endif /* INCLUDE_GLOBALS_H_ */
