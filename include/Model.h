/* Model.h */

#ifndef INCLUDE_MODEL_H_
#define INCLUDE_MODEL_H_

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/AgentRequest.h"
#include "repast_hpc/TDataSource.h"
#include "repast_hpc/SVDataSet.h"
#include "repast_hpc/SVDataSetBuilder.h"

#include <unordered_map>
#include <chrono>

#include "Agent.h"
#include "StatisticsCollector.h"
#include "StructuralEntity.h"
#include "SocialNetworkEntity.h"

/* Agent Package Provider */
class AgentPackageProvider {
	
private:
    repast::SharedContext<Agent>* agents;
	
public:
	
    AgentPackageProvider(repast::SharedContext<Agent>* agentPtr);
	
    void providePackage(Agent * agent, std::vector<AgentPackage>& out);
	
    void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);
	
};

/* Agent Package Receiver */
class AgentPackageReceiver {
	
private:
    repast::SharedContext<Agent>* agents;
	
public:
	
    AgentPackageReceiver(repast::SharedContext<Agent>* agentPtr);
	
    Agent * createAgent(AgentPackage package);
	
    void updateAgent(AgentPackage package);
	
};


class Model {
private:
	int stopAt;
	const int MECHANISM_START_TICK = 1;
	const int MECHANISM_INTERVAL_TICK = 1;
	const int YEARLY_START_TICK = 0;
	const int YEARLY_INTERVAL_TICK = 365;
	int SITUATIONAL_MECHANISM_INTERVAL_TICK = 1; //read from model.props
	int ACTION_MECHANISM_INTERVAL_TICK = 1; //read from model.props
	AgentPackageProvider* provider;
	AgentPackageReceiver* receiver;
	std::chrono::steady_clock::time_point startTime; //for timeout ERROR

	bool CUSTOM_LOG = false;
	int maxMemoryUsed = 0;

	std::string ANNUAL_DATA_FILE;
	bool NETWORK_FROM_FILE;
	
	int mIndexSex = -1;
	int mIndexAge = -1;
	int mIndexRace = -1;
	int mIndexDrinking = -1;
	int mIndexFrequencyLevel = -1;
	int mIndexMonthlyDrinks = -1;
	int mIndexParenthoodStatus = -1;
	int mIndexMaritalStatus = -1;
	int mIndexEmploymentStatus = -1;
	int mIndexIncome = -1;
	int mIndexSpawnTick = -1;
	int mIndexTraitImpulsivity = -1;
	int mIndexAnnualFrequency = -1;
	int mIndexGramsPerDay = -1;
	int mIndexHabitUpdateInterval = -1;

	int countDiePerYear = 0;
	int countMigrateOutPerYear = 0;
	int countSpawnPerYear = 0;

	double mAnnualHeavyDrinkingPrev; //Prevalence of heavy drinking in the population, update annually

	void getReadyToSpawn();
	void readMeanSDLookupTable(std::string lookupFilename);

	void readRoleTPTable(std::string tpFilename); //read role transition probability data
	std::string makeRoleTPHashKey(std::string year, std::string sex, std::string ageGroup, \
							   std::string currentMarriage, std::string currentParenthood, std::string currentEmployment, \
							   std::string nextMarriage, std::string nextParenthood, std::string NetxEmployment);//create hashkey for searching

	void readDeathRateTable(std::string filename); //read death rate
	std::string makeDeathRateHashKey(std::string year, std::string sex, std::string age);//create hashkey for searching

	void readMigrationOutTable(std::string filename); //read migration out
	std::string makeMigrationOutHashKey(std::string year, std::string sex, std::string race, std::string age);//create hashkey for searching

	void readSchemaProfilesTable(std::string filename); //read schema profiles for init
	std::string makeSchemaProfilesHashKey(std::string sex, std::string drinkerCategory);//create hashkey for searching

	void initNetworkFromEdgeListFile(std::string filename);
	std::vector<double> randomlyPickSchemaProfile(std::string sex, int monthlyDrinks);

	
protected:
	int countOfAgents;
	repast::Properties* props;
	repast::SharedContext<Agent> context;
	repast::SVDataSet* agentValues; // This is the data recording object
	repast::SVDataSet* annualValues;
	std::vector<StructuralEntity*> structuralEntityList; //a list of structural entity that perform transformational mechanisms
	std::queue<std::vector<std::string>> infoTable; //to store all individual info from file
	//std::queue<std::vector<std::vector<double>>> mortalityRates; //to yearly mortalityData, by sex & age group
	
	std::vector<std::string> readCSVRow(const std::string &row);
	std::queue<std::vector<std::string>> readCSV(std::istream &in);

	std::unordered_map<std::string, double> roleTransitionProbTable;//storing role transition probabilities
	std::unordered_multimap<std::string, std::vector<double>> schemaProfilesTable;//storing schema profiles for agent init
	
	int mIndexId = -1; //so spawning can used this to check for id
	int simYear;
	int simDay;//counting days from 0 to 364
	int startYear;

	int findIndexInHeader(std::string string, std::vector<std::string> headerLine);
	
	bool THEORY_SPECIFIC_OUTPUT;
	bool SPAWNING_ON;
	bool ROLE_TRANSITION_ON;
	bool PRINT_POPULATION_STAT_YEARLY;
	int SINGLE_RUN_LIMIT_MIN;
	
	StatisticsCollector* mpCollector;

	SocialNetworkEntity *mpSocialNetwork;


	
public:
	Model(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	virtual ~Model();
	void initialize(repast::ScheduleRunner& runner);
	void initAgents(); //create agents
	void initNetwork(); //init network
	void initSchedule(repast::ScheduleRunner& runner); //schedule 3 mechanisms and microsim time-related activities
	virtual void initStatisticCollector(); //theory-specific model class can override this to add statistic to annual_data.csv
	void addStatistics();

	void doDailyActions(); //all actions do every tick
	void doYearlyActions(); //all actions do every 365 ticks

	void doSituationalMechanisms();
	void doActionMechanisms();
	void doTransformationalMechanisms();
	
	void ageAgents(); // age all agents
	void spawnAgents(); // bring new adults into simulation.
	void killAgents(); // apply mortality rates to agents
	void incrementSimYear(); // go to the next year of simulation
	void countSimDay(); // counting day from 0 to 364
	void connectSpawnAgentToNetwork(Agent* agent); //connect agent to network (used with initNetwork)
	void removeLocalCopies(repast::AgentId agentId); //if theory maintains a copy of agents, use this to remove the copy when an agent is deleted
	
	void doRoleTransition(); //enable agent transition from one role status to another

	int getMaxMemoryUsed(){return maxMemoryUsed;} //get maximum memory use

	//these functions are implemented by theory-specific derived class
	//TODO: revise which virtual methods are required to be pure, i.e. must be overrided
	virtual void initMediatorAndTheoryWithRandomParameters(Agent *agent) = 0; //for an agent, create theory-specific mediator & theories with random variables
	virtual void initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) = 0; //for an agent, create theory-specific mediator & theories with variables read from file
	virtual void initForTheory(repast::ScheduleRunner& runner) = 0; //init and schedule data collection
	virtual void readRankFileForTheory(std::string rankFileName) = 0; //if before spawning, theory needs to get info from rank file	
	virtual void doYearlyTheoryActions() {}; //operations theory need to carry out each year
	virtual void addTheoryAnnualData(repast::SVDataSetBuilder& builder) {}; //add theory-specific outputs to builder for annual_data.csv
	virtual void writeDailyAgentDataToFile() {}; //1-core daily agent-level outputs
	virtual void writeAnnualTheoryDataToFile() {}; //1-core annual agent-level outputs
};

#endif
