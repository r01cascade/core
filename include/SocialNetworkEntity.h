#ifndef INCLUDE_SOCIAL_NETWORK_ENTITY_H_
#define INCLUDE_SOCIAL_NETWORK_ENTITY_H_

#include "StructuralEntity.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"
#include "Agent.h"
//#define DEBUG

//class Theory;

class SocialNetworkEntity : public StructuralEntity {

private:
	repast::SharedContext<Agent> 										*mpContext;
	repast::SharedNetwork<Agent, repast::RepastEdge<Agent>,	
						  repast::RepastEdgeContent<Agent>, 
						  repast::RepastEdgeContentManager<Agent> > 	*mpNetwork;					  
	double																mAverageOutdegree;
	double																mProportionTiesReciprocated;
	double																mMedianGeodesicDistance;
	double																mTransitivity;
	double 																mNetworkDensity;
	double 																mMoransIDrink;
	double																mMoransISex;
	double																mMoransIAge;	

public:
	SocialNetworkEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
			int transformationalInterval, repast::SharedContext<Agent> *context);
	~SocialNetworkEntity();

	void doTransformation() override;
	
	void connectAgentNetwork();
	void warmUpAgentNetwork(int warmUpTime);	
	void connectAgents(Agent* ego);

	std::vector<Agent*> getSuccessors(Agent* ego);
	std::vector<Agent*> getPredecessors(Agent* ego);
	void addSocialEdge(Agent* ego, Agent* alter);
	void removeSocialEdge(Agent* ego, Agent* alter);
	std::vector<Agent*> generateAgentPotentialBuddyList(Agent* ego);
	std::vector<Agent*> updatePotentialBuddies(std::vector<Agent*> potentialBuddies, Agent* ego, Agent* alter);


	//getters for network stats
	double getAverageOutdegree() 				{return mAverageOutdegree;}
	double getProportionTiesReciprocated()      {return mProportionTiesReciprocated;}
	double getMedianGeodesicDistance()          {return mMedianGeodesicDistance;}
	double getTransitivity() 					{return mTransitivity;}
	double getNetworkDensity()					{return mNetworkDensity;}
	double getMoransIDrink()					{return mMoransIDrink;}
	double getMoransISex()						{return mMoransISex;}
	double getMoransIAge()						{return mMoransIAge;}

	void calculateNetworkStats();

	double calculateAverageOutdegree();
	double calculateProportionTiesReciprocated();
	double calculateGeodesicDistance();
	double calculateTransitivityCoefficient();
	double calculateNetworkDensity(); 
	double calculateMoransIDrink(); 
	double calculateMoransISex();
	double calculateMoransIAge();

};
#endif	
