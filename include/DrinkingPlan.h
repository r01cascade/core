#ifndef DRINKING_PLAN
#define DRINKING_PLAN

#include <stdio.h>
#include <boost/mpi.hpp>

enum class DrinkingSchema { NONE=-1, ABSTAIN=0, LOW=1, MED=2, HIGH=3, VERY_HIGH=4 };

struct DrinkingPlan {
	DrinkingSchema schema;
	double probability;
};

#endif