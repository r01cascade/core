#ifndef INCLUDE_THEORY_H_
#define INCLUDE_THEORY_H_

class Agent; //forward declare, but DO NOT define

#include <utility>
#include "DrinkingPlan.h"

class Theory {

protected:
	Agent *mpAgent;

	double safeOdds(double numerator, double denominator);
	double safeLog(double value);

public:
	virtual ~Theory() {};

	void setAgent(Agent *agent);
	virtual void doSituation() = 0;
	virtual void doNonDrinkingActions() = 0;

	//correct mean and sd for ERFC function
	std::pair<double, double> generateCorrectedMeanSD (double desiredMean, double desiredSd);
	std::pair<double, double> doLookup(double mean, double sd);
	std::pair<double, double> doFunctionLookup(double desiredMean, double desiredSd);

	/* NEW ACTION MECH */
	virtual double getAttitude(DrinkingSchema schema) = 0;
	virtual double getNorm(DrinkingSchema schema) = 0;
	virtual double getPerceivedBehaviourControl(DrinkingSchema schema) = 0;
};

#endif /* INCLUDE_THEORY_H_ */
