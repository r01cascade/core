#ifndef INCLUDE_SOCIAL_NETWORK_REGULATOR_H_
#define INCLUDE_SOCIAL_NETWORK_REGULATOR_H_

#include "repast_hpc/SharedContext.h"

#include "Regulator.h"
#include "Agent.h"

class SocialNetworkRegulator : public Regulator {

private:
	repast::SharedContext<Agent> *mpContext;

public:
	SocialNetworkRegulator(repast::SharedContext<Agent> *context);
	~SocialNetworkRegulator();

	void updateAdjustmentLevel() override;
};

#endif