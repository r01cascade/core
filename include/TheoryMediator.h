#ifndef INCLUDE_THEORYMEDIATOR_H_
#define INCLUDE_THEORYMEDIATOR_H_

class Agent;
#include "Theory.h"
#include "DrinkingPlan.h"
#include "globals.h"
#include <vector>
#include <array>

class TheoryMediator {

protected:
	std::vector<Theory*> mTheoryList;
	Agent *mpAgent;

	/* NEW ACTION MECH */
	//mediated Attitude, Norm, PBC
	std::array<double, NUM_SCHEMA> mAttitude {};
	std::array<double, NUM_SCHEMA> mNorm {};
	std::array<double, NUM_SCHEMA> mPbc {};
	std::array<double, NUM_SCHEMA> mIntentionProb {};

public:
	TheoryMediator(std::vector<Theory*> theoryList);
	virtual ~TheoryMediator();
	void linkAgent(Agent *agent); //link agent to this mediator and all theories in the theory list

	template <typename derivedTheory>
	bool getTheory(derivedTheory** ppTheory) { //write a theory (from mTheoryList) with the matching type to derivedTheory, return true if success.
		for (std::vector<Theory*>::iterator it=mTheoryList.begin(); it!=mTheoryList.end(); ++it) {
				if (dynamic_cast<derivedTheory*>(*it)!=0) {
					*ppTheory = dynamic_cast<derivedTheory*>(*it);
					return true;
				}
			}
			return false;
	};

	virtual void mediateSituation() = 0;
	virtual void mediateAction() = 0;
	virtual void mediateNonDrinkingActions() = 0;
	
	/* NEW ACTION MECH */
	double getIntentionProbability(int i) {return mIntentionProb[i];}
	void setIntentionProbability(int i, double value) { mIntentionProb[i] = value;}
	virtual void mediateThoughtPathway() = 0; //must be overrided to mediate Attitude, Norm, PBC
	void updateIntentionProbabilities();
	DrinkingPlan getProportionalSelectionIntentionPlan();
	DrinkingPlan getMaxIntentionPlan();

	double getAttitude(int i) {return mAttitude[i];}
	double getNorm(int i) {return mNorm[i];}
	double getPbc(int i) {return mPbc[i];}
};

#endif /* INCLUDE_THEORYMEDIATOR_H_ */
