#ifndef INCLUDE_STATCOLLECTOR_H_
#define INCLUDE_STATCOLLECTOR_H_

#include <map>
#include <string>
#include <boost/variant.hpp>

#include "Agent.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/TDataSource.h"


template <class T> class StatDataSource; //forward declaration

class StatisticsCollector {
protected:
	std::map<std::string, boost::variant<int, double>> mMap;
	repast::SharedContext<Agent>* mpPopulation;

public:
	StatisticsCollector(repast::SharedContext<Agent>* pPopulation);
	virtual ~StatisticsCollector() { };

	bool exists(std::string);
	void collectAgentStatistics();
	virtual void collectTheoryAgentStatistics() {};
	
	template<typename T> void set(std::string key, T value) {
	  mMap[key] = value;
	}
	
	template<typename T> T get(std::string key) {
	  if (!exists(key))
		std::cerr << "Can't find key: " << key << std::endl;
	  return boost::get<T>(mMap.find(key)->second);
	}
	
	template<typename T> StatDataSource<T>* getDataSource(std::string name) {
		return (new StatDataSource<T>(this, name));
	}
};


template <class T>
class StatDataSource : public repast::TDataSource<T>{
private:
	StatisticsCollector* mpCollector;
	std::string mName;
public:
	StatDataSource(StatisticsCollector* pCollector, std::string name) {
		mpCollector = pCollector;
		mName = name;
	}
	
	T getData(){
		return mpCollector->get<T>(mName);
	}
};

#endif /* INCLUDE_STATCOLLECTOR_H_ */
