#include "SocialNetworkEntity.h"
#include "vector"
#include "globals.h"
#include "repast_hpc/Utilities.h"
#include "Theory.h"
#include <algorithm>
#include <iomanip>
#include "repast_hpc/Random.h"
#include "boost/graph/graph_traits.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/property_map/property_map.hpp"
#include "boost/graph/johnson_all_pairs_shortest.hpp"

//define DEBUGSN
//#define PRINTY

using namespace boost;

SocialNetworkEntity::SocialNetworkEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
		int transformationalInterval, repast::SharedContext<Agent> *context)
										: StructuralEntity(regulatorList, powerList, transformationalInterval) {
	repast::RepastEdgeContentManager<Agent> edgeContentManager;	
	//TODO Need to pass in initialized parameters for SharedNetwork. update 11/4/2019 what does this mean? 
	mpNetwork = new repast::SharedNetwork<Agent, repast::RepastEdge<Agent>,
										  repast::RepastEdgeContent<Agent>, 
										  repast::RepastEdgeContentManager<Agent> >
				("mpNetwork", true, &edgeContentManager);
	mpContext = context;
	mpContext->addProjection(mpNetwork);
}

SocialNetworkEntity::~SocialNetworkEntity(){}

void SocialNetworkEntity::connectAgentNetwork(){ //check if need boost::mpi::communicator* mpLocalcomm in input
	//old steps: initialize spatial projections, use reach, build networks.  
	//New steps: select two random agents, connect to them. initializet the potential buddies list.
	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
		Agent* ego = &**iter;
		connectAgents(ego);
	}
	//need to iterate through after initial network set up.  Hence new for loop.
	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
		Agent* ego = &**iter;
		ego->initPotentialBuddies();
	}
}

void SocialNetworkEntity::connectAgents(Agent* ego){
	std::vector<Agent*> initialTwoBuddies;
	initialTwoBuddies.push_back(ego);

	mpContext->selectAgents(2, initialTwoBuddies, true); //true removes "ego" from list.
	// Sort and shuffle the vector to address reproducibillity issues
	Agent::sortAndShuffleAgentPointers(initialTwoBuddies);

	for (std::vector<Agent*>::const_iterator buddy=initialTwoBuddies.begin(); buddy != initialTwoBuddies.end(); ++buddy){
		Agent* alter = &**buddy;
		addSocialEdge(ego, alter);
	}
	
	ego->setSocialNetwork(this);
}

void SocialNetworkEntity::warmUpAgentNetwork(int warmUpTime){
	for (int n = 0; n != warmUpTime; ++n){
		for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
			Agent* ego = &**iter;
			ego->selectDrinkingBuddies(AGE_SIMILARITY_FLAG,
										SEX_SIMILARITY_FLAG, 
										RACE_SIMILARITY_FLAG, 
										IS_DRINKING_TODAY_SIMILARITY_FLAG, 
										NUMBER_DRINKS_TODAY_SIMILARITY_FLAG, 
										IS_12_MONTH_DRINKERS_SIMILARITY_FLAG, 
										SELECTION_FREQUENCY_FLAG, 
										TOTAL_DRINKS_PER_ANNUM_SIMILARITY_FLAG, 
										MARITAL_STATUS_SIMILARITY_FLAG, 
										PARENTHOOD_STATUS_SIMILARITY_FLAG, 
										EMPLOYMENT_STATUS_SIMILARITY_FLAG, 
										MEAN_DRINKS_TODAY_SIMILARITY_FLAG, 
										SD_DRINKS_TODAY_SIMILARITY_FLAG, 
										INCOME_SIMILARITY_FLAG, 
										PAST_YEAR_N_SIMILARITY_FLAG, 
										SELECTION_QUANTITY_FLAG, 
										OUTDEGREE_FLAG, 
										RECIPROCITY_FLAG, 
										PREFERENTIAL_ATTACHMENT_FLAG, 
										TRANSITIVE_TRIPLES_FLAG,
										GPD_30_DAYS_FLAG);
		}
		//std::cout << "Warmup time: " << n << ". Average OutDegree: " << calculateAverageOutdegree() << std::endl;
	}
}

void SocialNetworkEntity::addSocialEdge(Agent* ego, Agent* alter){
	mpNetwork->addEdge(ego, alter);
}

void SocialNetworkEntity::removeSocialEdge(Agent* ego, Agent* alter){
	mpNetwork->removeEdge(ego, alter);
}

void SocialNetworkEntity::doTransformation(){
	//std::cout << "I guess I'm doing some transforming??" << std::endl;
	//no regulators operating at the social entity level
}

std::vector<Agent*> SocialNetworkEntity::getSuccessors(Agent* ego){
	std::vector<Agent*> outAlters;
	mpNetwork->successors(ego, outAlters);
	return outAlters;
}

std::vector<Agent*> SocialNetworkEntity::getPredecessors(Agent* ego){
	std::vector<Agent*> inAlters;
	mpNetwork->predecessors(ego, inAlters);
	return inAlters;
}

std::vector<Agent*> SocialNetworkEntity::generateAgentPotentialBuddyList(Agent* ego){
	//put buddies and self on potential buddies list, so can remove in select agents
	std::vector<Agent*> potentialBuddies = getSuccessors(ego);
	potentialBuddies.push_back(ego);
//	int numberToAdd = mpContext->size() - potentialBuddies.size();
	int numberToAdd = 50;
	mpContext->selectAgents(numberToAdd, potentialBuddies, true);
	// Sort and shuffle the vector to address reproducibillity issues
	Agent::sortAndShuffleAgentPointers(potentialBuddies);
	#ifdef PRINTY
	std::cout << "size of potentialBuddies: " << potentialBuddies.size() << std::endl;
	#endif
	return potentialBuddies;
}

std::vector<Agent*> SocialNetworkEntity::updatePotentialBuddies(std::vector<Agent*> potentialBuddies, Agent* ego, Agent* alter){
	repast::AgentId me = ego->getId();
	repast::AgentId you = alter->getId();
	
	//identify two new random people not on any list and not myself
	std::vector<Agent*> localPotentialBuddies = potentialBuddies;
	std::vector<Agent*> buddies = getSuccessors(ego);
	localPotentialBuddies.push_back(ego);
	for (std::vector<Agent*>::const_iterator it = buddies.begin(); it != buddies.end(); ++it){
		localPotentialBuddies.push_back(*it);
	}
	mpContext->selectAgents(2, localPotentialBuddies, true);
	// Sort and shuffle the vector to address reproducibillity issues
	Agent::sortAndShuffleAgentPointers(localPotentialBuddies);

	//remove 2 people, replace with 2 new random people
	if (potentialBuddies.size() > 1){
		potentialBuddies.erase(potentialBuddies.begin(), potentialBuddies.begin()+2);
	}
	for (std::vector<Agent*>::const_iterator i = localPotentialBuddies.begin(); i != localPotentialBuddies.end(); ++i){
		potentialBuddies.push_back(*i);
	}

	//remove alter from potential buddies list, if alter != ego.  Alter = ego if no friendship changes or a friend is dropped.
	if (me != you){
		potentialBuddies.erase(std::remove(potentialBuddies.begin(), potentialBuddies.end(), alter), potentialBuddies.end());
	}

	return(potentialBuddies);
}

void SocialNetworkEntity::calculateNetworkStats(){
	mAverageOutdegree = calculateAverageOutdegree();
	mProportionTiesReciprocated = calculateProportionTiesReciprocated();
	// mMedianGeodesicDistance = calculateGeodesicDistance();
	mTransitivity = calculateTransitivityCoefficient();
	mNetworkDensity = calculateNetworkDensity();
	mMoransIDrink = calculateMoransIDrink();
	mMoransISex = calculateMoransISex();
	mMoransIAge = calculateMoransIAge();
}

double SocialNetworkEntity::calculateAverageOutdegree(){
	double totalFriends = 0;
	int totalAgents = mpContext->size();

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){		
		Agent* ego = &**iter;
		std::vector<Agent*> myFriends = getSuccessors(ego);
		totalFriends += myFriends.size();
		}
	//TODO--do I want the distribution of these as well, or just totals/averages
	double averageOutdegree = totalFriends / totalAgents;
#ifdef DEBUGSN	
	std::cout << "rank " << repast::RepastProcess::instance()->rank() << " avg outdegree " << averageOutdegree << std::endl;
#endif	
	return averageOutdegree;
}
//this uses the "arc method" of calculating reciprocity.  In this method,  the number of reciprocated ties is
//divided by the total number of ties present in the system.  The ties A->B B->A are counted twice in the 
//numerator (A has a reciprocated tie to B, and B has a reciprocated tie to A), and in the denominator.
double SocialNetworkEntity::calculateProportionTiesReciprocated(){
	double totalFriends = 0;
	int totalAgents = mpContext->size();
	int totalReciprocatedTies = 0;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){		
		Agent* ego = &**iter;
	
		std::vector<Agent*> myFriends = getSuccessors(ego);
		std::vector<int> myFriendsIds;
		for (std::vector<Agent*>::const_iterator it = myFriends.begin(); it != myFriends.end(); ++it){
			int friendId = (*it)->getId().id();
			std::vector<int>::iterator up = std::upper_bound(myFriendsIds.begin(), myFriendsIds.end(), friendId);
			myFriendsIds.insert(up, friendId);
		}

		std::vector<Agent*> myInTies = getPredecessors(ego);
		std::vector<int> myInTiesIds;
		for (std::vector<Agent*>::const_iterator it = myInTies.begin(); it != myInTies.end(); ++it){
			int inTieId = (*it)->getId().id();
			std::vector<int>::iterator up = std::upper_bound(myInTiesIds.begin(), myInTiesIds.end(), inTieId);
			myInTiesIds.insert(up, inTieId);
		}

		std::vector<int> reciprocatedTies;
		std::set_intersection(myFriendsIds.begin(), myFriendsIds.end(), myInTiesIds.begin(), 
							  myInTiesIds.end(), back_inserter(reciprocatedTies));

		totalReciprocatedTies += reciprocatedTies.size();
		int friendSize = myFriends.size();
		totalFriends += friendSize;
		
	}
	double proportionTiesReciprocated = totalReciprocatedTies / totalFriends;
	return proportionTiesReciprocated;
}




//TODO--TUONG need everybody to loop through everybody to calculate this.
double SocialNetworkEntity::calculateGeodesicDistance(){
	//attempting to do this with a Boost library.  We shall see.....
	//1. create boost graph from current mpNetwork
	typedef adjacency_list<vecS, vecS, bidirectionalS, no_property, property<edge_weight_t, int> > Graph;
	typedef std::pair<int, int> Edge;
	std::vector<Edge> graphEdges;

	const int numVertex = mpContext->size();
	Graph mGraph(numVertex); //declare a graph object with size = all agents.
	//TODO: don't do this twice.  Either pass it in from above or put this in the calling method.
	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
		Agent* ego = &**iter;
		int egoId = ego->getId().id();
		std::vector<Agent*> myFriends = getSuccessors(ego);
		std::vector<int> myFriendsIds;
		for (std::vector<Agent*>::const_iterator its = myFriends.begin(); its != myFriends.end(); ++its){
			int friendId = (*its)->getId().id();
			std::vector<int>::iterator up = std::upper_bound(myFriendsIds.begin(), myFriendsIds.end(), friendId);
			myFriendsIds.insert(up, friendId);
		}
		for(std::vector<int>::const_iterator i = myFriendsIds.begin(); i != myFriendsIds.end(); ++i){
			graphEdges.push_back(Edge(egoId, *i));
		}
	}

	//also convert edges index from 1->n to 0->n-1
	#ifdef PRINTY
	std::cout << "edges are: " ;
	#endif
	for (std::vector<Edge>::const_iterator iter = graphEdges.begin(); iter != graphEdges.end(); ++iter){
		#ifdef PRINTY
		std::cout << "(" << (*iter).first-1 << ", " << (*iter).second-1 << ") ";
		#endif
		add_edge((*iter).first-1, (*iter).second-1, 1, mGraph);
	}
	std::vector<int> geoDistances(numVertex, (std::numeric_limits<int>::max)());

	//create a 2D array: geoDistanceMatrix[numVertex][numVertex]
	int** geoDistanceMatrix = new int*[numVertex];
	for(int i = 0; i < numVertex; ++i) {
		geoDistanceMatrix[i] = new int[numVertex]{};
	}

	//caculate all shortest paths
	johnson_all_pairs_shortest_paths(mGraph, geoDistanceMatrix, distance_map(&geoDistances[0]));

	double medianGeoDist;
	std::vector<int> nonInfiniteGeoDistances;
	#ifdef PRINTY
	std::cout << std::endl;
	std::cout << "All pairs shortest path: "<< std::endl;
	std::cout << "       ";
	for (int k =0; k < numVertex; ++k){
		std::cout << std::setw(5) << k;
	}
	std::cout << std::endl;
	#endif
	for (int i = 0; i < numVertex; ++i){
		#ifdef PRINTY
		std::cout << std::setw(3) << i << " -> ";
		#endif
		for (int j = 0; j < numVertex; ++j){
			if (geoDistanceMatrix[i][j] == (std::numeric_limits<int>::max)()){
				#ifdef PRINTY
				std::cout << std::setw(5) <<  "inf";
				#endif
			}else{
				if(geoDistanceMatrix[i][j] != 0){
					nonInfiniteGeoDistances.push_back(geoDistanceMatrix[i][j]);
				}
				#ifdef PRINTY
				std::cout << std::setw(5) << geoDistanceMatrix[i][j];
				#endif
			}
		}
		#ifdef PRINTY
		std::cout << std::endl;
		#endif
	}
	

	//finish with geoDistanceMatrix, delete from memory (since we use new to create this array)
	for(int i = 0; i < numVertex; ++i)
		delete[] geoDistanceMatrix[i];
	delete[] geoDistanceMatrix;


	size_t midIndex = nonInfiniteGeoDistances.size()/2;
	std::nth_element(nonInfiniteGeoDistances.begin(), nonInfiniteGeoDistances.begin() + midIndex, 
					 nonInfiniteGeoDistances.end());
	if (nonInfiniteGeoDistances.size() % 2 == 1){
		medianGeoDist = (double) nonInfiniteGeoDistances[midIndex];
	}else{
		medianGeoDist = ((double) nonInfiniteGeoDistances[midIndex] + nonInfiniteGeoDistances[midIndex - 1] ) /2;
	}
	#ifdef PRINTY
	std::cout << "median Geo Distance: " << medianGeoDist << std::endl;
	#endif
	return medianGeoDist;
	//2. use this boost graph to do all the stats, start with computing shortest path lengths
}	

double SocialNetworkEntity::calculateTransitivityCoefficient(){
	int totalOpenTriples = 0;
	int totalClosedTriples = 0;
	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
		Agent* ego = &**iter;
		#ifdef PRINTY
		std::cout << "me: " << ego->getId().id() << std::endl;
		#endif
		//find all open triples
		std::vector<Agent*> myBuddies = getSuccessors(ego);
		#ifdef PRINTY
		std::cout << "my buddies xij = 1: ";
		#endif
		for (std::vector<Agent*>::const_iterator it = myBuddies.begin(); it != myBuddies.end(); ++it){
			#ifdef PRINTY
			std::cout << (*it)->getId().id() << " ";
			#endif
			std::vector<Agent*> myBuddysBuddies = getSuccessors(*it);
			std::vector<Agent*> notEgoBuddysBuddies;
			for (std::vector<Agent*>::const_iterator iterBB = myBuddysBuddies.begin(); iterBB != myBuddysBuddies.end(); ++iterBB){
				if ((*iterBB)->getId().id() != ego->getId().id()){
					notEgoBuddysBuddies.push_back(*iterBB);
				}
				if (std::find(myBuddies.begin(), myBuddies.end(), *iterBB) != myBuddies.end()){
					++totalClosedTriples;
				}
			}
			int numBuddysBuddies = notEgoBuddysBuddies.size();
			#ifdef PRINTY
			std::cout << "and the number of friends they have: " << numBuddysBuddies << "; ";
			#endif
			totalOpenTriples += numBuddysBuddies;
		}
		#ifdef PRINTY
		std::cout << "total Open Triples = " << totalOpenTriples << std::endl;
		std::cout << "total Closed Triples: " << totalClosedTriples << std::endl;
		#endif
	}	
	return (double) totalClosedTriples / totalOpenTriples;
}


double SocialNetworkEntity::calculateNetworkDensity(){
	int totalFriends = 0;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){		
		Agent* ego = &**iter;
		std::vector<Agent*> myFriends = getSuccessors(ego);
		totalFriends += myFriends.size();
		}

	int totalPossibleLinks = mpContext->size() * (mpContext->size()-1);
	return ((double) totalFriends / totalPossibleLinks);
}

double SocialNetworkEntity::calculateMoransIDrink(){
	int N = mpContext->size();
	double numerator = 0.0;
	double squareTotalDiffrenceFromMean = 0.0;
	int totalFriends = 0;
	int totalDrinkFrequencyLevel = 0;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){		
		Agent* ego = &**iter;
		std::vector<Agent*> myFriends = getSuccessors(ego);
		totalFriends += myFriends.size();
		totalDrinkFrequencyLevel += ego->getDrinkFrequencyLevel();
	}
	double meanDrinkFrequencyLevel = (double) totalDrinkFrequencyLevel / N;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
		Agent* ego = &**iter;
		double myDrinkFrequencyLevel = (double) ego->getDrinkFrequencyLevel();
		double myDifferenceFromMean = myDrinkFrequencyLevel - meanDrinkFrequencyLevel;
		squareTotalDiffrenceFromMean += (myDifferenceFromMean * myDifferenceFromMean);
		std::vector<Agent*> myBuddies = getSuccessors(ego);
		for (std::vector<Agent*>::const_iterator it = myBuddies.begin(); it != myBuddies.end(); ++it){
			double buddyDrinkFrequency = (double) (*it)->getDrinkFrequencyLevel();
			numerator += myDifferenceFromMean * (buddyDrinkFrequency - meanDrinkFrequencyLevel);
		}
	}
	 return ((N * numerator) / (totalFriends * squareTotalDiffrenceFromMean));
}

double SocialNetworkEntity::calculateMoransISex(){
	int N = mpContext->size();
	double numerator = 0.0;
	double squareTotalDiffrenceFromMean = 0.0;
	int totalFriends = 0;
	int totalSex = 0;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){		
		Agent* ego = &**iter;
		std::vector<Agent*> myFriends = getSuccessors(ego);
		totalFriends += myFriends.size();
		totalSex += ego->getSex();
	}
	double meanSex = (double) totalSex / N;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
		Agent* ego = &**iter;
		double mySex = (double) ego->getSex();
		double myDifferenceFromMean = mySex - meanSex;
		squareTotalDiffrenceFromMean += (myDifferenceFromMean * myDifferenceFromMean);
		std::vector<Agent*> myBuddies = getSuccessors(ego);
		for (std::vector<Agent*>::const_iterator it = myBuddies.begin(); it != myBuddies.end(); ++it){
			double buddySex = (double) (*it)->getSex();
			numerator += myDifferenceFromMean * (buddySex - meanSex);
		}
	}
	 return ((N * numerator) / (totalFriends * squareTotalDiffrenceFromMean));
}

double SocialNetworkEntity::calculateMoransIAge(){
	int N = mpContext->size();
	double numerator = 0.0;
	double squareTotalDiffrenceFromMean = 0.0;
	int totalFriends = 0;
	int totalAge = 0;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){		
		Agent* ego = &**iter;
		std::vector<Agent*> myFriends = getSuccessors(ego);
		totalFriends += myFriends.size();
		totalAge += ego->getAge();
	}
	double meanAge = (double) totalAge / N;

	for (repast::SharedContext<Agent>::const_local_iterator iter = mpContext->localBegin(); iter != mpContext->localEnd(); ++iter){
		Agent* ego = &**iter;
		double myAge = (double) ego->getAge();
		double myDifferenceFromMean = myAge - meanAge;
		squareTotalDiffrenceFromMean += (myDifferenceFromMean * myDifferenceFromMean);
		std::vector<Agent*> myBuddies = getSuccessors(ego);
		for (std::vector<Agent*>::const_iterator it = myBuddies.begin(); it != myBuddies.end(); ++it){
			double buddyAge = (double) (*it)->getDrinkFrequencyLevel();
			numerator += myDifferenceFromMean * (buddyAge - meanAge);
		}
	}
	 return ((N * numerator) / (totalFriends * squareTotalDiffrenceFromMean));
}
