#include "Agent.h"
#include "Theory.h"
#include "globals.h"

#include <cmath>

TheoryMediator::TheoryMediator(std::vector<Theory*> theoryList) {
	mTheoryList = theoryList;
}

TheoryMediator::~TheoryMediator() {
	for(std::vector<Theory*>::iterator iter = mTheoryList.begin(); iter != mTheoryList.end(); iter++) {
		delete (*iter);
	}
}

void TheoryMediator::linkAgent(Agent *agent) {
	//link this mediator to agent
	mpAgent = agent;

	//link each theory to agent
	std::vector<Theory*>::iterator iter;
	for(iter = mTheoryList.begin(); iter != mTheoryList.end(); iter++) {
		(*iter)->setAgent(agent);
	}
}

/**
 * @details Calculate log odds for each schema, then calculate probability for each schema.
 */
void TheoryMediator::updateIntentionProbabilities() {
	//calculate logodds
	std::array<double, NUM_SCHEMA> logOdds = {};
	double totalLogOdds = 0;
	for (int i=0; i<NUM_SCHEMA; i++) {
		double temp = BETA_ATTITUDE * mAttitude[i] + BETA_NORM * mNorm[i] + BETA_PBC * mPbc[i];

		//deal with exp(temp) is out of range for double
		if (temp > log(std::numeric_limits<double>::max())) // temp > 709.783
			logOdds[i] = std::numeric_limits<double>::max();
		else if (temp < log(std::numeric_limits<double>::min())) // temp < -709.783
			logOdds[i] = std::numeric_limits<double>::min();
		else
			logOdds[i] = exp(temp);

		totalLogOdds += logOdds[i];
	}

	//calculate probabilities from logodds
	double total = 0;
	for (int i=0; i<NUM_SCHEMA; i++) {
		mIntentionProb[i] = (totalLogOdds==0) ? 0 : logOdds[i] / totalLogOdds;
	}
}


/**
 * @brief Do propotional selection to choose a schema.
 * 
 * @return The DrinkingPlan with schema and its associated probability
 */
DrinkingPlan TheoryMediator::getProportionalSelectionIntentionPlan() {
	//random a number from 0 -> total
	double total = 0;
	for (int i=0; i<NUM_SCHEMA; i++) {
		total += mIntentionProb[i];
	}
	if (total==0) {
		DrinkingPlan plan;
		plan.schema = DrinkingSchema::ABSTAIN;
		plan.probability = 1;
		return plan;
	}
	double rand = repast::Random::instance()->nextDouble() * total;
	
	//do the propotional selection
	DrinkingPlan plan;
	double cummulativeProb = 0;
	for (int i=0; i<NUM_SCHEMA; i++) {
		if (mIntentionProb[i] > 0) {
			cummulativeProb += mIntentionProb[i];
			if (rand <= cummulativeProb) {
				plan.probability = mIntentionProb[i];
				plan.schema = static_cast<DrinkingSchema>(i);
				if (plan.schema==DrinkingSchema::NONE)
					throw std::logic_error("MPI_ERR_OTHER");
				return plan;
			}
		}
	}
	// Address a control flow warning by returning a default or throwing an exception
	// Thorwing an exception here addresses to the 
	throw std::logic_error("Drinking plan not selected by TheoryMediator::getProportionalSelectionIntentionPlan");
}

/**
 * @brief Choose a schema with max probability.
 * 
 * @return The DrinkingPlan with schema and its associated probability
 */
DrinkingPlan TheoryMediator::getMaxIntentionPlan() {
	//search for max prob
	double maxIntentionProb = -1;
	int maxIntentionIndex = -1;
	for (int i=0; i<NUM_SCHEMA; i++)
		if (mIntentionProb[i] > maxIntentionProb) {
			maxIntentionProb = mIntentionProb[i];
			maxIntentionIndex = i;
		}

	DrinkingPlan plan;
	plan.schema = static_cast<DrinkingSchema>(maxIntentionIndex);
	plan.probability = maxIntentionProb;

	return plan;
}