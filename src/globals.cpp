/*
 * globals.cpp
 *
 *  Created on: 2017-11-7
 *      Author: alexxparrot
 */

#include "globals.h"
#include <map>
#include <utility>
#include <vector>

bool FAIL_FAST;
bool SOCIAL_NETWORK;

std::map<int, std::pair<double, double> > MEAN_SD_LOOKUP_TABLE;
std::unordered_map<std::string, double> deathRateTable;//storing death rate by characteristics
std::unordered_map<std::string, double> migrationOutTable;//storing migration-out rate by characteristics

/* NEW ACTION MECH */
double BETA_ATTITUDE = 1/3;
double BETA_NORM = 1/3;
double BETA_PBC = 1/3;
int HABIT_STOCK_DAYS = 60;
//int HABIT_UPDATE_INTERVAL = 180;

double AGE_SIMILARITY_BETA = 0;
double SEX_SIMILARITY_BETA = 0;
double RACE_SIMILARITY_BETA = 0;
double IS_DRINKING_TODAY_SIMILARITY_BETA = 0;
double NUMBER_DRINKS_TODAY_SIMILARITY_BETA = 0;
double IS_12_MONTH_DRINKERS_SIMILARITY_BETA = 0;
double SELECTION_FREQUENCY_BETA = 0;
double TOTAL_DRINKS_PER_ANNUM_SIMILARITY_BETA = 0;
double MARITAL_STATUS_SIMILARITY_BETA = 0;
double PARENTHOOD_STATUS_SIMILARITY_BETA = 0;
double EMPLOYMENT_STATUS_SIMILARITY_BETA = 0;
double MEAN_DRINKS_TODAY_SIMILARITY_BETA = 0;
double SD_DRINKS_TODAY_SIMILARITY_BETA = 0;
double INCOME_SIMILARITY_BETA = 0;
double PAST_YEAR_N_SIMILARITY_BETA = 0;
double SELECTION_QUANTITY_BETA = 0;
double OUTDEGREE_BETA = 0;
double RECIPROCITY_BETA = 0;
double PREFERENTIAL_ATTACHMENT_BETA = 0;
double TRANSITIVE_TRIPLES_BETA = 0;
double GPD_30_DAYS_BETA = 0;

bool AGE_SIMILARITY_FLAG = 0;
bool SEX_SIMILARITY_FLAG = 0;
bool RACE_SIMILARITY_FLAG = 0;
bool IS_DRINKING_TODAY_SIMILARITY_FLAG = 0;
bool NUMBER_DRINKS_TODAY_SIMILARITY_FLAG = 0;
bool IS_12_MONTH_DRINKERS_SIMILARITY_FLAG = 0;
bool SELECTION_FREQUENCY_FLAG = 0;
bool TOTAL_DRINKS_PER_ANNUM_SIMILARITY_FLAG = 0;
bool MARITAL_STATUS_SIMILARITY_FLAG = 0;
bool PARENTHOOD_STATUS_SIMILARITY_FLAG = 0;
bool EMPLOYMENT_STATUS_SIMILARITY_FLAG = 0;
bool MEAN_DRINKS_TODAY_SIMILARITY_FLAG = 0;
bool SD_DRINKS_TODAY_SIMILARITY_FLAG = 0;
bool INCOME_SIMILARITY_FLAG = 0;
bool PAST_YEAR_N_SIMILARITY_FLAG = 0;
bool SELECTION_QUANTITY_FLAG = 0;
bool OUTDEGREE_FLAG = 0;
bool RECIPROCITY_FLAG = 0;
bool PREFERENTIAL_ATTACHMENT_FLAG = 0;
bool TRANSITIVE_TRIPLES_FLAG = 0;
bool GPD_30_DAYS_FLAG = 0;

bool AGENT_LEVEL_OUTPUT = 0;

double UNIT_PRICE[40] = {
1.75, 1.69, 1.64, 1.64, 1.63, 1.61, 1.64, 1.66,
1.64, 1.63, 1.63, 1.67, 1.72, 1.71, 1.69, 1.65,
1.65, 1.65, 1.66, 1.67, 1.67, 1.65, 1.67, 1.67,
1.67, 1.66, 1.63, 1.63, 1.62, 1.69, 1.67, 1.66,
1.65, 1.64, 1.64, 1.66, 1.66, 1.64, 1.62, 1.63
}; //data from exogenous price table. 1980 through 2019 prices in 2019 dollars.
double TEMP_LEGAL_RISK = 0; //read in from model.props for now.
double DECAY_BASE_RATE = 0; // =0.001 constant from model.props
int DAYS_TO_DEVELOP_WITHDRAWAL = 0; //= 100; constant from model.props
int WITHDRAWAL_WASHOUT_DIVISOR = 0; //=4; constant from model.props
double HOURS_FREE_TIME_MEAN = 0; //4.94 constant from model.props
double HOURS_FREE_TIME_SD = 0; // 3.51 constant from model.props
int TOTAL_YEARS_CONSUMPTION_STOCK = 0; //constant from model.props
int HEAVY_DRINKS_PER_DAY = 0; //constant from model.props

bool ROLES_SOCIALISATION_ON = true;

double FREQUENCY_INFLUENCE_BETA = 0;
double QUANTITY_INFLUENCE_BETA= 0;
double INFLUENCE_LAMBDA= 0;
double SELECTION_LAMBDA= 0;