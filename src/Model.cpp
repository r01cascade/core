/* Model.cpp */

#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include <istream>
#include <string>
#include <cmath>
#include <unordered_map>

#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/Random.h"

#include "Model.h"
#include "globals.h"
#include "StatisticsCollector.h"

#include "Log.h"


AgentPackageProvider::AgentPackageProvider(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){ }

void AgentPackageProvider::providePackage(Agent * agent, std::vector<AgentPackage>& out){
    repast::AgentId id = agent->getId();
    AgentPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(), agent->getAge(), agent->getSex(), agent->isDrinkingToday(),
    	                 agent->getNumberDrinksToday(), agent->getDrinkFrequencyLevel(), agent->getPastYearDrinks());
    out.push_back(package);
}

void AgentPackageProvider::provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out){
    std::vector<repast::AgentId> ids = req.requestedAgents();
    for(size_t i = 0; i < ids.size(); i++){
        providePackage(agents->getAgent(ids[i]), out);
    }
}

AgentPackageReceiver::AgentPackageReceiver(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){}

Agent * AgentPackageReceiver::createAgent(AgentPackage package){
    repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
    return new Agent(id); // When agent moves to other process he/she only takes ID; all other properties will be reset
}

void AgentPackageReceiver::updateAgent(AgentPackage package){
    repast::AgentId id(package.id, package.rank, package.type);
    Agent * agent = agents->getAgent(id);
    agent->set(package.currentRank, package.age, package.sex, package.isDrinkingToday, package.numberDrinksToday,
    		   package.drinkFrequencyLevel, package.pastYearDrinks);
}


Model::Model(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm){
	props = new repast::Properties(propsFile, argc, argv, comm);
	stopAt = repast::strToInt(props->getProperty("stop.at"));
	countOfAgents = repast::strToInt(props->getProperty("count.of.agents"));
	startYear = repast::strToInt(props->getProperty("start.year"));
	simYear = 0;
	simDay = 0;

	provider = new AgentPackageProvider(&context);
	receiver = new AgentPackageReceiver(&context);
	startTime = std::chrono::steady_clock::now();

	//read flags and custom run settings
	THEORY_SPECIFIC_OUTPUT = repast::strToInt(props->getProperty("theory.specific.output"));
	AGENT_LEVEL_OUTPUT = repast::strToInt(props->getProperty("agent.level.output"));
	SPAWNING_ON = repast::strToInt(props->getProperty("spawning.on"));
	ROLE_TRANSITION_ON = repast::strToInt(props->getProperty("role.transition.on"));
	PRINT_POPULATION_STAT_YEARLY = repast::strToInt(props->getProperty("print.population.stat.yearly"));
	SINGLE_RUN_LIMIT_MIN = repast::strToInt(props->getProperty("single.run.limit.minute"));
	FAIL_FAST = repast::strToInt(props->getProperty("fail.fast.on"));
	NETWORK_FROM_FILE = props->getProperty("network.from.file").empty() ? 0 :repast::strToInt(props->getProperty("network.from.file"));
		
	//Read intervals for 3 mechanisms
	ACTION_MECHANISM_INTERVAL_TICK = repast::strToInt(props->getProperty("action.interval"));
	SITUATIONAL_MECHANISM_INTERVAL_TICK = repast::strToInt(props->getProperty("situational.interval"));

	//Read network set up (friend selection) betas
	AGE_SIMILARITY_BETA = props->getProperty("age.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("age.similarity.beta"));
	SEX_SIMILARITY_BETA = props->getProperty("sex.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("sex.similarity.beta"));
	RACE_SIMILARITY_BETA = props->getProperty("race.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("race.similarity.beta"));
	IS_DRINKING_TODAY_SIMILARITY_BETA = props->getProperty("is.drinking.today.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("is.drinking.today.similarity.beta"));
	NUMBER_DRINKS_TODAY_SIMILARITY_BETA = props->getProperty("number.drinks.today.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("number.drinks.today.similarity.beta"));
	IS_12_MONTH_DRINKERS_SIMILARITY_BETA = props->getProperty("is.12.month.drinkers.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("is.12.month.drinkers.similarity.beta"));
	SELECTION_FREQUENCY_BETA = props->getProperty("selection.frequency.beta").empty() ? 0 : repast::strToDouble(props->getProperty("selection.frequency.beta"));
	TOTAL_DRINKS_PER_ANNUM_SIMILARITY_BETA = props->getProperty("total.drinks.per.annum.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("total.drinks.per.annum.similarity.beta"));
	MARITAL_STATUS_SIMILARITY_BETA = props->getProperty("marital.status.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("marital.status.similarity.beta"));
	PARENTHOOD_STATUS_SIMILARITY_BETA = props->getProperty("parenthood.status.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("parenthood.status.similarity.beta"));
	EMPLOYMENT_STATUS_SIMILARITY_BETA = props->getProperty("employment.status.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("employment.status.similarity.beta"));
	MEAN_DRINKS_TODAY_SIMILARITY_BETA = props->getProperty("mean.drinks.today.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("mean.drinks.today.similarity.beta"));
	SD_DRINKS_TODAY_SIMILARITY_BETA = props->getProperty("sd.drinks.today.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("sd.drinks.today.similarity.beta"));
	INCOME_SIMILARITY_BETA = props->getProperty("income.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("income.similarity.beta"));
	PAST_YEAR_N_SIMILARITY_BETA = props->getProperty("past.year.n.similarity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("past.year.n.similarity.beta"));
	SELECTION_QUANTITY_BETA = props->getProperty("selection.quantity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("selection.quantity.beta"));
	OUTDEGREE_BETA = props->getProperty("outdegree.beta").empty() ? 0 : repast::strToDouble(props->getProperty("outdegree.beta"));
	RECIPROCITY_BETA = props->getProperty("reciprocity.beta").empty() ? 0 : repast::strToDouble(props->getProperty("reciprocity.beta"));
	PREFERENTIAL_ATTACHMENT_BETA = props->getProperty("preferential.attachment.beta").empty() ? 0 : repast::strToDouble(props->getProperty("preferential.attachment.beta"));
	TRANSITIVE_TRIPLES_BETA = props->getProperty("transitive.triples.beta").empty() ? 0 : repast::strToDouble(props->getProperty("transitive.triples.beta"));
	GPD_30_DAYS_BETA = props->getProperty("gpd.30.days.beta").empty() ? 0 : repast::strToDouble(props->getProperty("gpd.30.days.beta"));

	AGE_SIMILARITY_FLAG = props->getProperty("age.flag").empty() ? 0 : repast::strToDouble(props->getProperty("age.flag"));
	SEX_SIMILARITY_FLAG = props->getProperty("sex.flag").empty() ? 0 : repast::strToDouble(props->getProperty("sex.flag"));
	RACE_SIMILARITY_FLAG = props->getProperty("race.flag").empty() ? 0 : repast::strToDouble(props->getProperty("race.flag"));
	IS_DRINKING_TODAY_SIMILARITY_FLAG = props->getProperty("is.drinking.flag").empty() ? 0 : repast::strToDouble(props->getProperty("is.drinking.flag"));
	NUMBER_DRINKS_TODAY_SIMILARITY_FLAG = props->getProperty("number.drinks.today.flag").empty() ? 0 : repast::strToDouble(props->getProperty("number.drinks.today.flag"));
	IS_12_MONTH_DRINKERS_SIMILARITY_FLAG = props->getProperty("is.12.month.drinker.flag").empty() ? 0 : repast::strToDouble(props->getProperty("is.12.month.drinker.flag"));
	SELECTION_FREQUENCY_FLAG = props->getProperty("drink.frequency.level.flag").empty() ? 0 : repast::strToDouble(props->getProperty("drink.frequency.level.flag"));
	TOTAL_DRINKS_PER_ANNUM_SIMILARITY_FLAG = props->getProperty("total.drinks.per.annum.flag").empty() ? 0 : repast::strToDouble(props->getProperty("total.drinks.per.annum.flag"));
	MARITAL_STATUS_SIMILARITY_FLAG = props->getProperty("marital.status.flag").empty() ? 0 : repast::strToDouble(props->getProperty("marital.status.flag"));
	PARENTHOOD_STATUS_SIMILARITY_FLAG = props->getProperty("parenthood.status.flag").empty() ? 0 : repast::strToDouble(props->getProperty("parenthood.status.flag"));
	EMPLOYMENT_STATUS_SIMILARITY_FLAG = props->getProperty("employment.status.flag").empty() ? 0 : repast::strToDouble(props->getProperty("employment.status.flag"));
	MEAN_DRINKS_TODAY_SIMILARITY_FLAG = props->getProperty("mean.drinks.today.flag").empty() ? 0 : repast::strToDouble(props->getProperty("mean.drinks.today.flag"));
	SD_DRINKS_TODAY_SIMILARITY_FLAG = props->getProperty("sd.drinks.today.flag").empty() ? 0 : repast::strToDouble(props->getProperty("sd.drinks.today.flag"));
	INCOME_SIMILARITY_FLAG = props->getProperty("income.flag").empty() ? 0 : repast::strToDouble(props->getProperty("income.flag"));
	PAST_YEAR_N_SIMILARITY_FLAG = props->getProperty("past.year.n.flag").empty() ? 0 : repast::strToDouble(props->getProperty("past.year.n.flag"));
	SELECTION_QUANTITY_FLAG = props->getProperty("past.year.mean.drink.flag").empty() ? 0 : repast::strToDouble(props->getProperty("past.year.mean.drink.flag"));
	OUTDEGREE_FLAG = props->getProperty("outdegree.flag").empty() ? 0 : repast::strToDouble(props->getProperty("outdegree.flag"));
	RECIPROCITY_FLAG = props->getProperty("reciprocity.flag").empty() ? 0 : repast::strToDouble(props->getProperty("reciprocity.flag"));
	PREFERENTIAL_ATTACHMENT_FLAG = props->getProperty("preferential.attachment.flag").empty() ? 0 : repast::strToDouble(props->getProperty("preferential.attachment.flag"));
	TRANSITIVE_TRIPLES_FLAG = props->getProperty("transitive.triples.flag").empty() ? 0 : repast::strToDouble(props->getProperty("transitive.triples.flag"));
	GPD_30_DAYS_FLAG = props->getProperty("gpd.30.days.flag").empty() ? 0 : repast::strToDouble(props->getProperty("gpd.30.days.flag"));

	//read in ERFC lookup table
	std::string lookupFilename = props->getProperty("compressed.lookup.file");
	readMeanSDLookupTable(lookupFilename);

	//read role transition probability
	if (ROLE_TRANSITION_ON) {
		std::string tpFilename = props->getProperty("transition.probability.file");
		readRoleTPTable(tpFilename);
	}

	//read death rates
	std::string deathRateFilename = props->getProperty("death.rates.file");
	readDeathRateTable(deathRateFilename);

	//read migration out rates
	std::string migrationOutFilename = props->getProperty("migration.out.rates.file");
	readMigrationOutTable(migrationOutFilename);

	//read schema profiles
	std::string schemaProfilesFilename = props->getProperty("schema.profiles.file");
	readSchemaProfilesTable(schemaProfilesFilename);

	//custom-logging flag
	CUSTOM_LOG = props->getProperty("custom.log").empty() ? 0 : repast::strToInt(props->getProperty("custom.log"));
	if (CUSTOM_LOG) {
		Logger("Starting...");
	}

	//full path of annual data file
	ANNUAL_DATA_FILE = props->getProperty("annual.data.file").empty() ? "./outputs/annual_data.csv" : props->getProperty("annual.data.file");

	//social network (default: off)
	SOCIAL_NETWORK = props->getProperty("social.network").empty() ? 0 : repast::strToInt(props->getProperty("social.network"));

	//beta for calculating intention
	BETA_ATTITUDE = props->getProperty("beta.attitude").empty() ? 1/3 : repast::strToDouble(props->getProperty("beta.attitude"));
	BETA_NORM = props->getProperty("beta.norm").empty() ? 1/3 : repast::strToDouble(props->getProperty("beta.norm"));
	BETA_PBC = props->getProperty("beta.pbc").empty() ? 1/3 : repast::strToDouble(props->getProperty("beta.pbc"));
	
	HABIT_STOCK_DAYS = props->getProperty("habit.stock.days").empty() ? 60 : repast::strToDouble(props->getProperty("habit.stock.days"));
	//HABIT_UPDATE_INTERVAL = props->getProperty("habit.update.interval").empty() ? 180 : repast::strToDouble(props->getProperty("habit.update.interval"));
}

void Model::readRoleTPTable(std::string tpFilename) {

	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(tpFilename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << tpFilename << std::endl;
	}

	//find index on header line.
	std::vector<std::string> headerLine = localTable.front();
	localTable.pop();

	int indexYear = findIndexInHeader("year", headerLine);
	int indexSex = findIndexInHeader("sex", headerLine);
	int indexAge = findIndexInHeader("age", headerLine);
	int indexCurrentM = findIndexInHeader("currentMarriage", headerLine);
	int indexCurrentP = findIndexInHeader("currentParenthood", headerLine);
	int indexCurrentE = findIndexInHeader("currentEmployment", headerLine);
	int indexNextM = findIndexInHeader("nextMarriage", headerLine);
	int indexNextP = findIndexInHeader("nextParenthood", headerLine);
	int indexNextE = findIndexInHeader("nextEmployment", headerLine);
	int indexTP = findIndexInHeader("tp", headerLine);

	//read in variables and create map stored in globals.
	std::string localKey;
	double localTP;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<std::string> localMap = localTable.front();
		localTable.pop();
	
		//read the variables from a row of localTable and make hashkey
		localKey = makeRoleTPHashKey(localMap[indexYear], localMap[indexSex], localMap[indexAge], \
								  localMap[indexCurrentM], localMap[indexCurrentP], localMap[indexCurrentE], \
								  localMap[indexNextM], localMap[indexNextP], localMap[indexNextE]);
		localTP = repast::strToDouble(localMap[indexTP]);
		//create a key/pair map
		roleTransitionProbTable.insert({localKey,localTP});
		
	}
}

std::string Model::makeRoleTPHashKey(std::string year, std::string sex, std::string age, \
							      std::string currentMarriage, std::string currentParenthood, std::string currentEmployment, \
							      std::string nextMarriage, std::string nextParenthood, std::string nextEmployment) {

	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + age + currentMarriage + currentParenthood + currentEmployment + nextMarriage + nextParenthood + nextEmployment;
	
	return std::to_string(tp_hash(hash_index));
}

void Model::readMeanSDLookupTable(std::string lookupFilename) {

	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(lookupFilename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open lookup file: " << lookupFilename << std::endl;
	}

	//find index on header line.
	std::vector<std::string> headerLine = localTable.front();
	localTable.pop();

	int indexKey = findIndexInHeader("hashkey", headerLine);
	int indexMean = findIndexInHeader("ERFC.mean", headerLine);
	int indexSd = findIndexInHeader("ERFC.SD", headerLine);
	
	//read in variables and create map stored in globals.
	int localKey;
	double localMean;
	double localSd;
	std::pair<double, double> localMeanSd;
	
	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<std::string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable
		localKey = repast::strToInt(localMap[indexKey]);
		localMean = repast::strToDouble(localMap[indexMean]);
		localSd = repast::strToDouble(localMap[indexSd]);
		localMeanSd = std::make_pair(localMean, localSd);
		//create a key/pair map from row.
		
		MEAN_SD_LOOKUP_TABLE.insert(std::make_pair(localKey, localMeanSd));
	
	}

}

Model::~Model(){
	if (CUSTOM_LOG) {
		Logger("Maximum memory used: " + std::to_string(getMaxMemoryUsed()/1024) + "MB");//logging maximum memory use
	}
	
	delete props;
	delete provider;
	delete receiver;
	delete annualValues;
	for (std::vector<StructuralEntity*>::iterator it=structuralEntityList.begin(); it!= structuralEntityList.end(); ++it)
		delete (*it);

	if (CUSTOM_LOG) {
		auto elapsed = std::chrono::duration_cast<std::chrono::seconds>( std::chrono::steady_clock::now() - startTime );
		Logger("End - runtime: " + std::to_string(elapsed.count()) + "sec");//logging runtime
	}

	if(mpCollector != nullptr) {
		delete mpCollector;
		mpCollector = nullptr;
	}
}

int Model::findIndexInHeader(std::string string, std::vector<std::string> headerLine) {
	std::vector<std::string>::iterator it = std::find(headerLine.begin(), headerLine.end(), string);
	if (it == headerLine.end()) {
		std::cout << "Index Not Found: " << string << std::endl;
		return -1;
	}

	// Found: return index of element from iterator
	return std::distance(headerLine.begin(), it);
}

void Model::initialize(repast::ScheduleRunner& runner) {
	initAgents(); //Generate Agents (core)
	initStatisticCollector(); //init (either here or overwrite by theory-specific model class)
	addStatistics(); //add annual data collection
	if (SOCIAL_NETWORK) initNetwork(); //init network
	initSchedule(runner); //manange schedule (core)
	initForTheory(runner); //manage and schedule data collection (theory specific)
}

// Generating agents, theories, structural entities
void Model::initAgents(){
	double monthlyDrinksSDPct = repast::strToDouble(props->getProperty("microsim.init.monthly.sd.pct"));

	//find index from the header line, ASSUMING the infoTable was already be read
	std::vector<std::string> headerLine = infoTable.front();
	infoTable.pop();

	mIndexId = findIndexInHeader("microsim.init.id", headerLine);
	mIndexSex = findIndexInHeader("microsim.init.sex", headerLine);
	mIndexAge = findIndexInHeader("microsim.init.age", headerLine);
	mIndexRace = findIndexInHeader("microsim.init.race", headerLine);
	mIndexDrinking = findIndexInHeader("microsim.init.drinkingstatus", headerLine);
	//mIndexFrequencyLevel = findIndexInHeader("microsim.init.drink.frequency", headerLine);
	mIndexMonthlyDrinks = findIndexInHeader("microsim.init.drinks.per.month", headerLine);
	mIndexMaritalStatus = findIndexInHeader("microsim.roles.marital.status", headerLine);
	mIndexParenthoodStatus = findIndexInHeader("microsim.roles.parenthood.status", headerLine);
	mIndexEmploymentStatus = findIndexInHeader("microsim.roles.employment.status", headerLine);
	mIndexIncome = findIndexInHeader("microsim.init.income", headerLine);
	mIndexTraitImpulsivity = findIndexInHeader("trait.impulsivity", headerLine);
	mIndexAnnualFrequency = findIndexInHeader("microsim.init.annual.frequency", headerLine);
	mIndexGramsPerDay = findIndexInHeader("microsim.init.alc.gpd", headerLine);
	mIndexHabitUpdateInterval = findIndexInHeader("habit.update.interval", headerLine);

	//read in variables and create agents
	int tempId;
	bool sex;
	int age;
	std::string race;
	int drinking;
	int drinkFrequencyLevel;
	int monthlyDrinks;
	int maritalStatus;
	int parenthoodStatus;
	int employmentStatus;
	int income;
	int year = simYear + startYear;
	double traitImpulsivity;
	int annualFrequency;
	double gramsPerDay;
	int habitUpdateInterval;

	//GET-INIT-DRINKS
	// FILE* initFile = fopen ("drinks.csv","w");
	// fprintf(initFile,"tick,id,sex,thday,drinks\n");
	// double tick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();

	//ignore first line, and assume the rest of lines = countOfAgents in model.props
	while (!infoTable.empty() && context.size() < countOfAgents) {
		std::vector<std::string> info = infoTable.front();
		infoTable.pop();

		//read the variables from a row of infoTable
		tempId = repast::strToInt(info[mIndexId]);
		sex = repast::strToInt(info[mIndexSex]);
		age = repast::strToInt(info[mIndexAge]);
		race = info[mIndexRace];
		drinking = repast::strToInt(info[mIndexDrinking]);
		//drinkFrequencyLevel = repast::strToInt(info[mIndexFrequencyLevel]);
		monthlyDrinks = repast::strToInt(info[mIndexMonthlyDrinks]);
		maritalStatus = repast::strToInt(info[mIndexMaritalStatus]);
		parenthoodStatus = repast::strToInt(info[mIndexParenthoodStatus]);
		employmentStatus = repast::strToInt(info[mIndexEmploymentStatus]);
		income = repast::strToInt(info[mIndexIncome]);
		traitImpulsivity = repast::strToDouble(info[mIndexTraitImpulsivity]);
		annualFrequency = repast::strToInt(info[mIndexAnnualFrequency]);
		gramsPerDay = repast::strToDouble(info[mIndexGramsPerDay]);
		habitUpdateInterval = repast::strToDouble(info[mIndexHabitUpdateInterval]);
		
		//from days per year frequency to frequency level 1-7
		if (annualFrequency<=10) drinkFrequencyLevel = 1;
		else if (annualFrequency<=18) drinkFrequencyLevel = 2;
		else if (annualFrequency<=30) drinkFrequencyLevel = 3;
		else if (annualFrequency<=53) drinkFrequencyLevel = 4;
		else if (annualFrequency<=171) drinkFrequencyLevel = 5;
		else if (annualFrequency<=336) drinkFrequencyLevel = 6;
		else drinkFrequencyLevel = 7;

		//create an agent using a constructor with variables from file
		int rank = repast::RepastProcess::instance()->rank();
		repast::AgentId id(tempId, rank, 0);
		id.currentRank(rank);
		Agent *agent = new Agent(id, sex, age, race, maritalStatus, parenthoodStatus, employmentStatus, 
				income, drinking, drinkFrequencyLevel, monthlyDrinks, monthlyDrinksSDPct, 
				year, traitImpulsivity, annualFrequency, gramsPerDay, habitUpdateInterval);
		context.addAgent(agent);

		//init mediators and theory
		initMediatorAndTheoryFromFile(agent, info);

		//GET-INIT-DRINKS
		// std::vector<int> drinksVector = agent->getPastYearDrinks();
		// for (int i=0; i<365; i++)
		// 	fprintf(initFile,"%.1f,%d,%d,%d,%d\n",tick,tempId,sex,i,drinksVector[i]);
	}

	//GET-INIT-DRINKS
	// fclose(initFile);

	//Init agents complete. Overwrite infoTable with spawn file data
	getReadyToSpawn();
}

void Model::initStatisticCollector() {
	mpCollector = new StatisticsCollector(&context);
}

void Model::addStatistics() {
	std::string fileOutputName(ANNUAL_DATA_FILE);
	repast::SVDataSetBuilder builder(fileOutputName.c_str(), ",", repast::RepastProcess::instance()->getScheduleRunner().schedule());

	StatDataSource<int>* outSumPopulation = mpCollector->getDataSource<int>("Population");
	builder.addDataSource(repast::createSVDataSource("Population", outSumPopulation, std::plus<int>()));

	StatDataSource<int>* outSumMale = mpCollector->getDataSource<int>("Male");
	builder.addDataSource(repast::createSVDataSource("Male", outSumMale, std::plus<int>()));

	StatDataSource<int>* outSumFemale = mpCollector->getDataSource<int>("Female");
	builder.addDataSource(repast::createSVDataSource("Female", outSumFemale, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup1 = mpCollector->getDataSource<int>("AgeGroup1");
	builder.addDataSource(repast::createSVDataSource("AgeGroup1", outSumAgeGroup1, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup2 = mpCollector->getDataSource<int>("AgeGroup2");
	builder.addDataSource(repast::createSVDataSource("AgeGroup2", outSumAgeGroup2, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup3 = mpCollector->getDataSource<int>("AgeGroup3");
	builder.addDataSource(repast::createSVDataSource("AgeGroup3", outSumAgeGroup3, std::plus<int>()));

	StatDataSource<int>* outSumAgeGroup4 = mpCollector->getDataSource<int>("AgeGroup4");
	builder.addDataSource(repast::createSVDataSource("AgeGroup4", outSumAgeGroup4, std::plus<int>()));


	StatDataSource<int>* outSum12MonthDrinkers = mpCollector->getDataSource<int>("12MonthDrinkers");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkers", outSum12MonthDrinkers, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersMale = mpCollector->getDataSource<int>("12MonthDrinkersMale");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersMale", outSum12MonthDrinkersMale, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersFemale = mpCollector->getDataSource<int>("12MonthDrinkersFemale");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersFemale", outSum12MonthDrinkersFemale, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup1 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup1");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup1", outSum12MonthDrinkersAgeGroup1, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup2 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup2");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup2", outSum12MonthDrinkersAgeGroup2, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup3 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup3");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup3", outSum12MonthDrinkersAgeGroup3, std::plus<int>()));

	StatDataSource<int>* outSum12MonthDrinkersAgeGroup4 = mpCollector->getDataSource<int>("12MonthDrinkersAgeGroup4");
	builder.addDataSource(repast::createSVDataSource("12MonthDrinkersAgeGroup4", outSum12MonthDrinkersAgeGroup4, std::plus<int>()));


	StatDataSource<double>* outSumQuantMale = mpCollector->getDataSource<double>("QuantMale");
	builder.addDataSource(repast::createSVDataSource("QuantMale", outSumQuantMale, std::plus<double>()));

	StatDataSource<double>* outSumQuantFemale = mpCollector->getDataSource<double>("QuantFemale");
	builder.addDataSource(repast::createSVDataSource("QuantFemale", outSumQuantFemale, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup1 = mpCollector->getDataSource<double>("QuantAgeGroup1");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup1", outSumQuantAgeGroup1, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup2 = mpCollector->getDataSource<double>("QuantAgeGroup2");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup2", outSumQuantAgeGroup2, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup3 = mpCollector->getDataSource<double>("QuantAgeGroup3");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup3", outSumQuantAgeGroup3, std::plus<double>()));

	StatDataSource<double>* outSumQuantAgeGroup4 = mpCollector->getDataSource<double>("QuantAgeGroup4");
	builder.addDataSource(repast::createSVDataSource("QuantAgeGroup4", outSumQuantAgeGroup4, std::plus<double>()));


	StatDataSource<int>* outSumFreqMale = mpCollector->getDataSource<int>("FreqMale");
	builder.addDataSource(repast::createSVDataSource("FreqMale", outSumFreqMale, std::plus<int>()));

	StatDataSource<int>* outSumFreqFemale = mpCollector->getDataSource<int>("FreqFemale");
	builder.addDataSource(repast::createSVDataSource("FreqFemale", outSumFreqFemale, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup1 = mpCollector->getDataSource<int>("FreqAgeGroup1");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup1", outSumFreqAgeGroup1, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup2 = mpCollector->getDataSource<int>("FreqAgeGroup2");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup2", outSumFreqAgeGroup2, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup3 = mpCollector->getDataSource<int>("FreqAgeGroup3");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup3", outSumFreqAgeGroup3, std::plus<int>()));

	StatDataSource<int>* outSumFreqAgeGroup4 = mpCollector->getDataSource<int>("FreqAgeGroup4");
	builder.addDataSource(repast::createSVDataSource("FreqAgeGroup4", outSumFreqAgeGroup4, std::plus<int>()));


	StatDataSource<int>* outSumOccasionalHeavyDrinking = mpCollector->getDataSource<int>("SumOccasionalHeavyDrinking");
	builder.addDataSource(repast::createSVDataSource("SumOccasionalHeavyDrinking", outSumOccasionalHeavyDrinking, std::plus<int>()));

	addTheoryAnnualData(builder);

	annualValues = builder.createDataSet();
}

void Model::getReadyToSpawn() {
	//read in file for spawning info table
	int rank = repast::RepastProcess::instance()->rank();
	std::string rankFileNameProperty = "spawn.file.rank" + std::to_string(rank);
	std::string rankFileName = props->getProperty(rankFileNameProperty);
	std::string spawnFileName = rankFileName;
	readRankFileForTheory(spawnFileName);

	//find index from the header line, ASSUMING the infoTable was already be read
	std::vector<std::string> headerLine = infoTable.front();
	infoTable.pop();

	mIndexId = findIndexInHeader("microsim.init.id", headerLine);
	mIndexSex = findIndexInHeader("microsim.init.sex", headerLine);
	mIndexAge = findIndexInHeader("microsim.init.age", headerLine);
	mIndexRace = findIndexInHeader("microsim.init.race", headerLine);
	mIndexDrinking = findIndexInHeader("microsim.init.drinkingstatus", headerLine);
	mIndexFrequencyLevel = findIndexInHeader("microsim.init.drink.frequency", headerLine);
	mIndexMonthlyDrinks = findIndexInHeader("microsim.init.drinks.per.month", headerLine);
	mIndexMaritalStatus = findIndexInHeader("microsim.roles.marital.status", headerLine);
	mIndexParenthoodStatus = findIndexInHeader("microsim.roles.parenthood.status", headerLine);
	mIndexEmploymentStatus = findIndexInHeader("microsim.roles.employment.status", headerLine);
	mIndexIncome = findIndexInHeader("microsim.init.income", headerLine);
	mIndexSpawnTick = findIndexInHeader("microsim.spawn.tick", headerLine);
	mIndexTraitImpulsivity = findIndexInHeader("trait.impulsivity", headerLine);
	mIndexAnnualFrequency = findIndexInHeader("microsim.init.annual.frequency", headerLine);
	mIndexGramsPerDay = findIndexInHeader("microsim.init.alc.gpd", headerLine);
	mIndexHabitUpdateInterval = findIndexInHeader("habit.update.interval", headerLine);
}

void Model::doSituationalMechanisms(){
	//Rank 0 checks elapsed duration and throws an error if needed
	if (SINGLE_RUN_LIMIT_MIN > 0) {
		if (repast::RepastProcess::instance()->rank() == 0) {
			auto elapsed = std::chrono::duration_cast<std::chrono::minutes>( std::chrono::steady_clock::now() - startTime );
			if (elapsed.count() > SINGLE_RUN_LIMIT_MIN) {
				std::cerr << "Model. The simulation has run for more than " << SINGLE_RUN_LIMIT_MIN << " minutes." << std::endl;
				throw std::logic_error("MPI_ERR_QUOTA");
			}
		}
	}

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//Here is where the agents are looped, agent memories can be revised here
		//Both situational mechanisms and the action mechanism are called
		//Agents update in a random order

		(*iter)->doSituation();
		
		iter++;
	}
	
	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doActionMechanisms(){
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {

		(*iter)->doAction();

		iter++;
	}
	
	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::doTransformationalMechanisms() {
	std::vector<StructuralEntity*>::iterator it = structuralEntityList.begin();
	while(it != structuralEntityList.end()){

		(*it)->doTransformation();
		
		it++;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

void Model::ageAgents(){
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//Age each agent in turn

		(*iter)->ageAgent();
		(*iter)->reset12MonthDrinker();
		(*iter)->resetTotalDrinksPerAnnum();

		iter++;
    }

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::killAgents(){
	// int year = 0;
	// bool agentSex = FEMALE;
	// int agentAge = 0;
	// std::string agentRace = "";

	// std::string hashkey;
	double agentMortality = 0.0;
	bool agentLives = false;
	double migrationOutRate = 0.0;
	bool agentMigration = false;

	std::vector<repast::AgentId> agentKillList;

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();

	while (iter != iterEnd) {
		if ((*iter)->getAge() >= 81) { //kill off agents >= 81 years old
			repast::AgentId id = (*iter)->getId();
			agentKillList.push_back(id);
			countDiePerYear++;
		} else {
			// Get required characteristics
			// year = simYear + startYear;
			// agentSex = (*iter)->getSex();
			// agentAge = (*iter)->getAge();
			// agentRace = (*iter)->getRace();

			// Look up mortality rate
			// hashkey = makeDeathRateHashKey(std::to_string(year), std::to_string(agentSex), std::to_string(agentAge));
			// agentMortality = deathRateTable[hashkey];
			agentMortality = (*iter)->getMortalityRate();
			// Apply risk
			agentLives = (repast::Random::instance()->nextDouble() > agentMortality/365);

			// Flag this agent to die
			if(agentLives == false && countOfAgents > 1){
				repast::AgentId id = (*iter)->getId();
				agentKillList.push_back(id);
				countDiePerYear++;
			} else { //check migration out if this agent is not flagged to kill
				// Look up migration out rate
				// hashkey = makeMigrationOutHashKey(std::to_string(year), std::to_string(agentSex), agentRace, std::to_string(agentAge));
				// migrationOutRate = migrationOutTable[hashkey];
				migrationOutRate = (*iter)->getMigrationOutRate();
				// Apply risk
				agentMigration = (repast::Random::instance()->nextDouble() > migrationOutRate/365);

				// Flag this agent to migrate out
				if(agentMigration == false && countOfAgents > 1){
					repast::AgentId id = (*iter)->getId();
					agentKillList.push_back(id);
					countMigrateOutPerYear++;
				}
			}
		}
		iter++;
	}


	for (repast::AgentId agentId : agentKillList) {
		//remove local copies of this agent in all other agents. TODO: re-check for multi-core removal.
		removeLocalCopies(agentId);

		// Remove agent
		repast::RepastProcess::instance()->agentRemoved(agentId);
		context.removeAgent(agentId);

		// Update counts + records of agents
		countOfAgents--;
	}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}


void Model::incrementSimYear(){
	simYear++;
	ageAgents();
}

void Model::countSimDay(){
	simDay++; 
	if (simDay==YEARLY_INTERVAL_TICK) simDay = 0;
	//std::cout<<"Day = "<<simDay<<std::endl;
}

void Model::spawnAgents(){
	int tempId;
	bool sex;
	int age;
	std::string race;
	int drinking;
	int drinkFrequencyLevel;
	int monthlyDrinks;
	double monthlyDrinksSDPct = repast::strToDouble(props->getProperty("microsim.init.monthly.sd.pct"));
	int maritalStatus;
	int parenthoodStatus;
	int employmentStatus;
	int income;
	int year = simYear + startYear;
	double traitImpulsivity;
	int annualFrequency;
	double gramsPerDay;
	int habitUpdateInterval;

	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());

	if (!infoTable.empty())
		while (repast::strToInt(infoTable.front()[mIndexSpawnTick]) <= currentTick) {
			std::vector<std::string> info = infoTable.front();
			infoTable.pop();

			//read the variables from a row of infoTable
			tempId = repast::strToInt(info[mIndexId]);
			sex = repast::strToInt(info[mIndexSex]);
			age = repast::strToInt(info[mIndexAge]);
			race = info[mIndexRace];
			drinking = repast::strToInt(info[mIndexDrinking]);
			drinkFrequencyLevel = repast::strToInt(info[mIndexFrequencyLevel]);
			monthlyDrinks = repast::strToInt(info[mIndexMonthlyDrinks]);
			maritalStatus = repast::strToInt(info[mIndexMaritalStatus]);
			parenthoodStatus = repast::strToInt(info[mIndexParenthoodStatus]);
			employmentStatus = repast::strToInt(info[mIndexEmploymentStatus]);
			income = repast::strToInt(info[mIndexIncome]);
			traitImpulsivity = repast::strToDouble(info[mIndexTraitImpulsivity]);
			annualFrequency = repast::strToInt(info[mIndexAnnualFrequency]);
			gramsPerDay = repast::strToDouble(info[mIndexGramsPerDay]);
			habitUpdateInterval = repast::strToDouble(info[mIndexHabitUpdateInterval]);
			
			//from days per year frequency to frequency level 1-7
			if (annualFrequency<=10) drinkFrequencyLevel = 1;
			else if (annualFrequency<=18) drinkFrequencyLevel = 2;
			else if (annualFrequency<=30) drinkFrequencyLevel = 3;
			else if (annualFrequency<=53) drinkFrequencyLevel = 4;
			else if (annualFrequency<=171) drinkFrequencyLevel = 5;
			else if (annualFrequency<=336) drinkFrequencyLevel = 6;
			else drinkFrequencyLevel = 7;

			//create an agent using a constructor with variables from file
			int rank = repast::RepastProcess::instance()->rank();
			repast::AgentId id(tempId, rank, 0);
			id.currentRank(rank);
			Agent *agent = new Agent(id, sex, age, race, maritalStatus, parenthoodStatus, employmentStatus, 
					income, drinking, drinkFrequencyLevel, monthlyDrinks, monthlyDrinksSDPct, 
					year, traitImpulsivity, annualFrequency, gramsPerDay, habitUpdateInterval);
			context.addAgent(agent);

			//init mediators and theory
			initMediatorAndTheoryFromFile(agent, info);
			if (SOCIAL_NETWORK) connectSpawnAgentToNetwork(agent);

			//doSituation once when the agent spawn into the simulation
			agent->doSituation();

			// Update counts + records of agents
			countSpawnPerYear++;
			countOfAgents++;

			if (infoTable.empty()) break;
		}

	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
}

// void Model::reset12MonthDrinkers() {
// 	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
// 	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
// 	while (iter != iterEnd) {
// 		//Age each agent in turn

// 		(*iter)->reset12MonthDrinker();

// 		iter++;
// 	}

// 	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
// }

// //added by Hao
// void Model::resetTotalDrinksPerAnnum() {
// 	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
// 	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
// 	while (iter != iterEnd) {
// 		//Age each agent in turn

// 		(*iter)->resetTotalDrinksPerAnnum();

// 		iter++;
// 	}

// 	repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(*provider, *receiver);
// }

void Model::doRoleTransition() {
	int year;
	int age;
	int sex;
	int currentMarriageStatus;
	int currentParenthoodStatus;
	int currentEmploymentStatus;
	int nextMarriageStatus;
	int nextParenthoodStatus;
	int nextEmploymentStatus;
	int dec_current;
	int dec_next;
	std::vector<double> probabilityRange;//numbers of possible status (hard coded for 3 roles)
	int nextStatus;
	int roleChangeStatus;
	std::string hashkey;
	double tp;
	double sumTP = 0;
	double randomNum;

	//modify transition probability when lead to more roles
	double TPBeta1 = (props->getProperty("role.tp.beta1").empty() ? 0 : repast::strToDouble(props->getProperty("role.tp.beta1")));
	
	//modify transition probability when lead to less roles
	double TPBeta2 = (props->getProperty("role.tp.beta2").empty() ? 0 : repast::strToDouble(props->getProperty("role.tp.beta2")));

	double tempBeta;

	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getDayOfRoleTransition()==simDay){
			currentMarriageStatus = (*iter)->getMaritalStatus();
			currentParenthoodStatus = (*iter)->getParenthoodStatus();
			currentEmploymentStatus = (*iter)->getEmploymentStatus();

			//convert binary to decimal
			std::string dec_string = std::to_string(currentMarriageStatus)+ std::to_string(currentParenthoodStatus)+ std::to_string(currentEmploymentStatus);
			dec_current = std::stoi(dec_string,nullptr,2);
			
			//get year, sex, age_group, current status
			year = simYear + startYear;//hard coded for starting from 1980 (put it in model.props?)
			age = (*iter)->getAge();
			sex = (*iter)->getSex();
			probabilityRange.push_back(0);
			int indexProbabilityRange = 1;
			int index_dec_current = -1;

			//testing roles transition by agent ID
			//repast::AgentId AgentID = (*iter)->getId();
			//if (AgentID.id() == 100)
			//	std::cout<<"Agent "<<"transition day = "<<(*iter)->getDayOfRoleTransition()<<" simDay ="<<simDay<<" M = "<<currentMarriageStatus<<" P = "<<currentParenthoodStatus<<" E = "<<currentEmploymentStatus<<std::endl;

			//create probability ranges for 8 targets
			for (nextMarriageStatus = 0; nextMarriageStatus < 2; nextMarriageStatus++){
				for(nextParenthoodStatus = 0; nextParenthoodStatus < 2; nextParenthoodStatus++){
					for (nextEmploymentStatus = 0; nextEmploymentStatus < 2; nextEmploymentStatus++){
						//making hashkey
						hashkey = makeRoleTPHashKey(std::to_string(year), std::to_string(sex), std::to_string(age), \
												std::to_string(currentMarriageStatus), std::to_string(currentParenthoodStatus), std::to_string(currentEmploymentStatus), \
												std::to_string(nextMarriageStatus), std::to_string(nextParenthoodStatus), std::to_string(nextEmploymentStatus));
					
						tp = roleTransitionProbTable[hashkey];
						//generate decimal for next status
						std::string dec_string_next = std::to_string(nextMarriageStatus)+ std::to_string(nextParenthoodStatus)+ std::to_string(nextEmploymentStatus);
						dec_next = std::stoi(dec_string_next,nullptr,2);

						//which beta: more or less
						if (dec_next > dec_current){
							//leading to more roles
							//tp = tp*(1 - TPBeta1*(*iter)->isHaveKDrinksOverNDays(30, 5));
							tempBeta = -TPBeta1;
						}
						else if (dec_next < dec_current){
							//leading to less roles
							//tp = tp*(1 + TPBeta2*(*iter)->isHaveKDrinksOverNDays(30, 5));
							tempBeta = TPBeta2;
						}
						else {
							index_dec_current = indexProbabilityRange;
							tempBeta = 0;
						}

						//update transition rate
						if ((*iter)->getNumDaysHavingKDrinksOverNDays(30,5) > 0) //heavy drinkers
							tp = tp*(1+tempBeta);
						else
							tp = tp*(1-mAnnualHeavyDrinkingPrev*(1+tempBeta))/(1-mAnnualHeavyDrinkingPrev);

						if (tp < 0) {
							std::cerr << "Model - Role transition. Transition probability is negative." << std::endl;
							throw std::logic_error("MPI_ERR_OTHER");
						}
						sumTP = sumTP+tp;
						probabilityRange.push_back(sumTP);
						indexProbabilityRange++;
					}
				}
			}

			//Adjust tp
			//if any tp negative, throw error (above)
			//if sumTp < 1, increase transition to the same number of roles so sum = 1
			//if sumTp > 1, normalise
			if (sumTP < 1) {
				//adjust from the index of the current to the end of the array (because this is an accumulated sum array)
				for (int i = index_dec_current; i < static_cast<int>(probabilityRange.size()); i++)
					probabilityRange[i] += 1 - sumTP;
				sumTP = 1;
			} else if (sumTP > 1) {
				//normalise
				for(int i = 0; i < static_cast<int>(probabilityRange.size()); i++){
					if (probabilityRange.back() != 0)
						probabilityRange[i] = probabilityRange[i]/probabilityRange.back();
				}
			}

			//generating random value
			randomNum = repast::Random::instance()->nextDouble();

			//getting region a random double value fall in (in [0,1]) and set as next role combi
			for (int index = 1; index < 9; index++){
				if (randomNum > probabilityRange[index-1] && randomNum <= probabilityRange[index]){
					nextStatus = index - 1;

					nextMarriageStatus = nextStatus/4;
					if (nextStatus >= 4) {
						nextParenthoodStatus = (nextStatus - 4) / 2;
						nextEmploymentStatus = (nextStatus - 4) % 2;
					}
					else {
						nextParenthoodStatus = nextStatus / 2;
						nextEmploymentStatus = nextStatus % 2;
					}

					if (dec_current > index - 1)
						roleChangeStatus = -1;
					else if (dec_current < index - 1)
						roleChangeStatus = 1;
					else 
						roleChangeStatus = 0;

					(*iter)->setMaritalStatus(nextMarriageStatus);
					(*iter)->setParenthoodStatus(nextParenthoodStatus);
					(*iter)->setEmploymentStatus(nextEmploymentStatus);
					
					//set up status
					std::string dec_string_next = std::to_string(nextMarriageStatus)+ \
												std::to_string(nextParenthoodStatus)+ \
												std::to_string(nextEmploymentStatus);
					
					dec_next = std::stoi(dec_string_next,nullptr,2);

					if (dec_next == dec_current) (*iter)->setRoleChangeStatus(0);
					else {
						if (dec_next > dec_current) (*iter)->setRoleChangeStatus(1);
						else (*iter)->setRoleChangeStatus(-1);
						(*iter)->setRoleChangedTransient(true);
					}
				
					break;
				}
			}

			probabilityRange.clear();
			sumTP = 0;
		}
		else (*iter)->setRoleChangedTransient(false);

		iter++;
    }
}

void Model::doDailyActions() {
	countSimDay();

	int currentTick = (int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (currentTick==MECHANISM_START_TICK || currentTick % SITUATIONAL_MECHANISM_INTERVAL_TICK == 0) {
		doSituationalMechanisms();
	}
	if (currentTick % ACTION_MECHANISM_INTERVAL_TICK == 0) {
		doActionMechanisms();
	}
	doTransformationalMechanisms();
	if (ROLE_TRANSITION_ON) doRoleTransition();
	if (THEORY_SPECIFIC_OUTPUT) writeDailyAgentDataToFile();
	killAgents();
	if (SPAWNING_ON) spawnAgents();
}

void Model::doYearlyActions() {
	double dblCurrentTick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
	
	//only collect data on tick = 0
	if (dblCurrentTick == 0) { //the first date of the first year
		mpCollector->collectAgentStatistics();
		annualValues->record();
		doYearlyTheoryActions();
	}

	if (dblCurrentTick > 0) { //the last date of each year
		mpCollector->collectAgentStatistics();
		annualValues->record();
		//if (THEORY_SPECIFIC_OUTPUT) writeAnnualTheoryDataToFile();
		incrementSimYear();
		// reset12MonthDrinkers();
		// resetTotalDrinksPerAnnum();
		doYearlyTheoryActions();
	}

	//Calculate prevalence of heavy drinking in the population - for role selection
	int countDrinkers = 0;
	int countHeavyDrinkers = 0;
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		if ((*iter)->isHaveKDrinksOverNDays(365, 1)) {
			countDrinkers += 1;
			if ((*iter)->getNumDaysHavingKDrinksOverNDays(30,5) > 0) countHeavyDrinkers += 1;
		}
		iter++;
	}
	if (countDrinkers == 0)
		mAnnualHeavyDrinkingPrev = 0;
	else mAnnualHeavyDrinkingPrev = (double)countHeavyDrinkers / (double)countDrinkers;


	if (PRINT_POPULATION_STAT_YEARLY) {
		std::cout << "Year " << startYear+simYear << "\t" << countOfAgents
				<< "\tDie " << countDiePerYear
				<< "\tMigrate-Out " << countMigrateOutPerYear
				<< "\tSpawn " << countSpawnPerYear
				<< std::endl;
	}

	//reset pop stat count
	countDiePerYear = 0;
	countMigrateOutPerYear = 0;
	countSpawnPerYear = 0;

	//logging memory use
	if (CUSTOM_LOG) {
		int memoryUsed = getUsedMemoryValue();
		if (memoryUsed > maxMemoryUsed) maxMemoryUsed = memoryUsed;
		Logger("Physical memory in use: " + std::to_string(memoryUsed/1024)+"MB");
	}
}

void Model::initSchedule(repast::ScheduleRunner& runner){
	runner.scheduleEvent(MECHANISM_START_TICK, MECHANISM_INTERVAL_TICK, \
			repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doDailyActions)));

	runner.scheduleEvent(YEARLY_START_TICK, YEARLY_INTERVAL_TICK, \
						repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::doYearlyActions)));

	runner.scheduleStop(stopAt);

	runner.scheduleEndEvent(repast::Schedule::FunctorPtr(new repast::MethodFunctor<repast::DataSet>(annualValues, &repast::DataSet::write)));
}

//states of a character in CSV file
enum class CSVState {
    UnquotedField,
    QuotedField,
    QuotedQuote
};

//read on row in the CSV file
std::vector<std::string> Model::readCSVRow(const std::string &row) {
    CSVState state = CSVState::UnquotedField;
    std::vector<std::string> fields {""};
    size_t i = 0; // index of the current field
    for (char c : row) {
        switch (state) {
            case CSVState::UnquotedField:
                switch (c) {
                    case ',': // end of field
                              fields.push_back(""); i++;
                              break;
                    case '"': state = CSVState::QuotedField;
                              break;
                    default:  fields[i].push_back(c);
                              break; }
                break;
            case CSVState::QuotedField:
                switch (c) {
                    case '"': state = CSVState::QuotedQuote;
                              break;
                    default:  fields[i].push_back(c);
                              break; }
                break;
            case CSVState::QuotedQuote:
                switch (c) {
                    case ',': // , after closing quote
                              fields.push_back(""); i++;
                              state = CSVState::UnquotedField;
                              break;
                    case '"': // "" -> "
                              fields[i].push_back('"');
                              state = CSVState::QuotedField;
                              break;
                    default:  // end of quote
                              state = CSVState::UnquotedField;
                              break; }
                break;
        }
    }
    return fields;
}

// Read CSV file, Excel dialect. Accept "quoted fields ""with quotes"""
std::queue<std::vector<std::string>> Model::readCSV(std::istream &in) {
    std::queue<std::vector<std::string>> table;
    std::string row;
    while (!in.eof()) {
        std::getline(in, row);
        if (in.bad() || in.fail()) {
            break;
        }
        auto fields = readCSVRow(row);
        table.push(fields);
    }
    return table;
}

void Model::readDeathRateTable(std::string filename) {
	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(filename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << filename << std::endl;
	}

	//find index on header line.
	std::vector<std::string> headerLine = localTable.front();
	localTable.pop();

	int indexYear = findIndexInHeader("microsim.init.year", headerLine);
	int indexSex = findIndexInHeader("microsim.init.sex", headerLine);
	int indexAge = findIndexInHeader("microsim.init.age", headerLine);
	int indexTP = findIndexInHeader("death.rate", headerLine);

	//read in variables and create map stored in globals.
	std::string localKey;
	double localTP;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<std::string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable and make hashkey
		localKey = makeDeathRateHashKey(localMap[indexYear], localMap[indexSex], localMap[indexAge]);
		localTP = repast::strToDouble(localMap[indexTP]);
		//create a key/pair map
		deathRateTable.insert({localKey,localTP});
	}
}

std::string Model::makeDeathRateHashKey(std::string year, std::string sex, std::string age) {
	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + age;
	return std::to_string(tp_hash(hash_index));
}

void Model::readMigrationOutTable(std::string filename) {
	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(filename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << filename << std::endl;
	}

	//find index on header line.
	std::vector<std::string> headerLine = localTable.front();
	localTable.pop();

	int indexYear = findIndexInHeader("microsim.init.year", headerLine);
	int indexSex = findIndexInHeader("microsim.init.sex", headerLine);
	int indexRace = findIndexInHeader("microsim.init.race", headerLine);
	int indexAge = findIndexInHeader("microsim.init.age", headerLine);
	int indexTP = findIndexInHeader("migration.out.rate", headerLine);

	//read in variables and create map stored in globals.
	std::string localKey;
	double localTP;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<std::string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable and make hashkey
		localKey = makeMigrationOutHashKey(localMap[indexYear], localMap[indexSex], localMap[indexRace], localMap[indexAge]);
		localTP = repast::strToDouble(localMap[indexTP]);
		//create a key/pair map
		migrationOutTable.insert({localKey,localTP});
	}
}

std::string Model::makeMigrationOutHashKey(std::string year, std::string sex, std::string race, std::string age) {
	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + race + age;
	return std::to_string(tp_hash(hash_index));
}

void Model::readSchemaProfilesTable(std::string filename) {
	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(filename);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << filename << std::endl;
	}

	//find index on header line.
	std::vector<std::string> headerLine = localTable.front();
	localTable.pop();

	int indexSex = findIndexInHeader("sex", headerLine);
	int indexDrinkerCat = findIndexInHeader("drinkerCAT", headerLine);
	int indexSchema1 = findIndexInHeader("schema1", headerLine);
	int indexSchema2 = findIndexInHeader("schema2", headerLine);
	int indexSchema3 = findIndexInHeader("schema3", headerLine);
	int indexSchema4 = findIndexInHeader("schema4", headerLine);
	int indexSchema5 = findIndexInHeader("schema5", headerLine);
	
	//read in variables and create map stored in globals.
	std::string localKey;
	double localTP;

	//read each line and put into a global table
	while (!localTable.empty()) {
		std::vector<std::string> localMap = localTable.front();
		localTable.pop();

		//read the variables from a row of localTable and make hashkey
		localKey = makeSchemaProfilesHashKey(localMap[indexSex], localMap[indexDrinkerCat]);
		std::vector<double> localVector = {
			repast::strToDouble(localMap[indexSchema1]),
			repast::strToDouble(localMap[indexSchema2]),
			repast::strToDouble(localMap[indexSchema3]),
			repast::strToDouble(localMap[indexSchema4]),
			repast::strToDouble(localMap[indexSchema5])
		};

		//create a key/pair map
		schemaProfilesTable.insert({localKey,localVector});
	}
}

std::string Model::makeSchemaProfilesHashKey(std::string sex, std::string drinkerCategory) {
	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = sex + drinkerCategory;
	return std::to_string(tp_hash(hash_index));
}

void Model::initNetwork(){
	//crete social network entity
	//This will be index 0 in the Structural Entity List - other specific need to be careful when accessing the list via index
	std::vector<Regulator*> regulatorList;
	std::vector<double> powerList;
	mpSocialNetwork = new SocialNetworkEntity(regulatorList, powerList, 1, &context);
	structuralEntityList.push_back(mpSocialNetwork);

	//reading social network from file?
	if (NETWORK_FROM_FILE){
		std::string edgeListFileName = props->getProperty("edgelist.file");
		initNetworkFromEdgeListFile(edgeListFileName);
	}else{
		mpSocialNetwork->connectAgentNetwork();
		mpSocialNetwork->warmUpAgentNetwork(50);
	}
	//std::cout << "done init network" << std::endl;
	//std::cout << "Average OutDegree: " << mpSocialNetwork->calculateAverageOutdegree() << std::endl;
}

void Model::initNetworkFromEdgeListFile(std::string edgeListFileName){
	//read from csv and store in localTable
	std::queue<std::vector<std::string>> localTable;

	ifstream myfile(edgeListFileName);
	if (myfile.is_open()) {
		//read the csv file
		localTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << edgeListFileName << std::endl;
	}

	while (!localTable.empty()) {
		std::vector<std::string> localMap = localTable.front();
		localTable.pop();
		repast::AgentId egoId(repast::strToInt(localMap[0]),repast::strToInt(localMap[1]),repast::strToInt(localMap[2]),repast::strToInt(localMap[3]));
		repast::AgentId alterId(repast::strToInt(localMap[4]),repast::strToInt(localMap[5]),repast::strToInt(localMap[6]),repast::strToInt(localMap[7]));
		//std::cout << egoId << "," << alterId << std::endl;
		Agent* ego = context.getAgent(egoId);
		Agent* alter = context.getAgent(alterId);
		mpSocialNetwork->addSocialEdge(ego, alter);
	}
}


void Model::connectSpawnAgentToNetwork(Agent *agent) {
	//connect to the network
	mpSocialNetwork->connectAgents(agent);

	//get ContagionTheory in the agent and call initPotentialBuddies() function
	agent->initPotentialBuddies();
	//std::cout << "done  connectSpawnAgentToNetwork" << std::endl;
	int warmUpTime = 7; //7 allows the agent to drop 2 initial ties, and add up to 5.
	for (int n = 0; n != warmUpTime; ++n){
		agent->selectDrinkingBuddies(AGE_SIMILARITY_FLAG,
									SEX_SIMILARITY_FLAG, 
									RACE_SIMILARITY_FLAG, 
									IS_DRINKING_TODAY_SIMILARITY_FLAG, 
									NUMBER_DRINKS_TODAY_SIMILARITY_FLAG, 
									IS_12_MONTH_DRINKERS_SIMILARITY_FLAG, 
									SELECTION_FREQUENCY_FLAG, 
									TOTAL_DRINKS_PER_ANNUM_SIMILARITY_FLAG, 
									MARITAL_STATUS_SIMILARITY_FLAG, 
									PARENTHOOD_STATUS_SIMILARITY_FLAG, 
									EMPLOYMENT_STATUS_SIMILARITY_FLAG, 
									MEAN_DRINKS_TODAY_SIMILARITY_FLAG, 
									SD_DRINKS_TODAY_SIMILARITY_FLAG, 
									INCOME_SIMILARITY_FLAG, 
									PAST_YEAR_N_SIMILARITY_FLAG, 
									SELECTION_QUANTITY_FLAG, 
									OUTDEGREE_FLAG, 
									RECIPROCITY_FLAG, 
									PREFERENTIAL_ATTACHMENT_FLAG, 
									TRANSITIVE_TRIPLES_FLAG,
									GPD_30_DAYS_FLAG);
	}
}

void Model::removeLocalCopies(repast::AgentId agentId) {
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		if ((*iter)->getId().id()!=agentId.id())
			(*iter)->removeAgentFromBuddiesLists(agentId);
		iter++;
	}
}

std::vector<double> Model::randomlyPickSchemaProfile(std::string sex, int monthlyDrinks) {
	//convert monthly drink to grams per days to find drinker category
	double gramsPerDay = (double)monthlyDrinks*14.0/30;
	std::string drinkerCategory;
	if (sex==to_string(FEMALE)) {
		if (gramsPerDay < 20)
			drinkerCategory = "LOW";
		else if (gramsPerDay < 40)
			drinkerCategory = "MED";
		else if (gramsPerDay < 60)
			drinkerCategory = "HIGH";
		else
			drinkerCategory = "VERYHIGH";
	} else {
		if (gramsPerDay < 40)
			drinkerCategory = "LOW";
		else if (gramsPerDay < 70)
			drinkerCategory = "MED";
		else if (gramsPerDay < 100)
			drinkerCategory = "HIGH";
		else
			drinkerCategory = "VERYHIGH";
	}
	std::string key = makeSchemaProfilesHashKey(sex, drinkerCategory);
	
	//randomly pick a profile
	int countProfiles = schemaProfilesTable.count(key);
	repast::IntUniformGenerator intGen = repast::Random::instance()->createUniIntGenerator(0,countProfiles-1);
	int selectedProfileIndex = intGen.next();

	//extract profile
	auto iterators = schemaProfilesTable.equal_range(key);
	int tempIndex = 0;
	for (auto it = iterators.first; it != iterators.second; ++it) {
		if (tempIndex == selectedProfileIndex) {
			return it->second;
		} else {
			tempIndex++;
		}
	}
	// Address control flow warning for the impossible case where the selecteProfileIndex > countProfiles-1.
	throw std::logic_error("Invalid random profile index in Model::randomlyPickSchemaProfile");
}
