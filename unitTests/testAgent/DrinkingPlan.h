#ifndef DRINKING_PLAN
#define DRINKING_PLAN

enum class DrinkingSchema { NONE, ABSTAIN, LOW, MED, HIGH, VERY_HIGH };

struct DrinkingPlan {
	DrinkingSchema schema;
	double probability;

    void utSet_schema( DrinkingSchema ut_schema)
        { schema = ut_schema; }

    void utSet_probability( double ut_probability)
        { probability = ut_probability; }

    DrinkingSchema utGet_schema( ) { return schema; }

    double utGet_probability( ) { return probability; }

};

#endif