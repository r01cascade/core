// This file was created automatically by coreMakeTests.py

#include <string>
#include <iostream>
#include <chrono>
#include <random>
#include <ctime>
#define IN_RANGE
#include "Agent.h"
#include "Theory.h"
#include "globals.h"
#include <cmath>

void test_TheoryMediator_1(int nTimes){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_THEORYMEDIATOR_1

#ifdef TEST_THEORYMEDIATOR_1


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.


       std::cout << "Before using constructor\n";
       TheoryMediator thisClass = TheoryMediator(
		/* std::vector<Theory* */ > theoryList);
       std::cout << "After using constructor\n";

    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_THEORYMEDIATOR_1
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_TheoryMediator_1 elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_TheoryMediator_linkAgent(int nTimes, TheoryMediator thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_THEORYMEDIATOR_LINKAGENT
//#define TEST_THEORYMEDIATOR_LINKAGENT_IN_RANGE

#ifdef TEST_THEORYMEDIATOR_LINKAGENT
    Agent my_agent;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_agent = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// TheoryMediator.cpp: void TheoryMediator::linkAgent(Agent *agent) {
// TheoryMediator.cpp: 	//link this mediator to agent
// TheoryMediator.cpp: 	mpAgent = agent;
// TheoryMediator.cpp: 
// TheoryMediator.cpp: 	//link each theory to agent
// TheoryMediator.cpp: 	std::vector<Theory*>::iterator iter;
// TheoryMediator.cpp: 	for(iter = mTheoryList.begin(); iter != mTheoryList.end(); iter++) {
// TheoryMediator.cpp: 		(*iter)->setAgent(agent);
// TheoryMediator.cpp: 	}
// TheoryMediator.cpp: }


       std::cout << "Before using TheoryMediator\n";
       result_0 = thisClass.linkAgent(my_agent);
       std::cout << "After using TheoryMediator\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_THEORYMEDIATOR_LINKAGENT_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_THEORYMEDIATOR_LINKAGENT
#ifdef TEST_THEORYMEDIATOR_LINKAGENT_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_TheoryMediator_linkAgent elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_TheoryMediator_getIntention(int nTimes, TheoryMediator thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
#define TEST_THEORYMEDIATOR_GETINTENTION
//#define TEST_THEORYMEDIATOR_GETINTENTION_IN_RANGE

#ifdef TEST_THEORYMEDIATOR_GETINTENTION

    DrinkingPlan resultDrinkingPlan;
    DrinkingPlan expectedDrinkingPlan;

    double resultProbability;
    double expectedProbabilityArray[5];
    
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        int m = m + 1;
        thisClass.utSet_mAttitude(m);
        thisClass.utSet_mNorm(m*2);
        thisClass.utSet_mPbc(m*3);

          BETA_A_1=1;   BETA_N_1=2;   BETA_PBC_1=3;
          BETA_A_2=2;   BETA_N_2=4;   BETA_PBC_2=6;
          BETA_A_3=3;   BETA_N_3=6;   BETA_PBC_3=9;
          BETA_A_4=4;   BETA_N_4=8;   BETA_PBC_4=12;
          BETA_A_5=5;   BETA_N_5=10;   BETA_PBC_5=15;

        double logOdds1 = log(1*m+2*2*m+3*3*m);
        double logOdds2 = log(2*(1*m+2*2*m+3*3*m));
        double logOdds3 = log(3*(1*m+2*2*m+3*3*m));
        double logOdds4 = log(4*(1*m+2*2*m+3*3*m));
        double logOdds5 = log(5*(1*m+2*2*m+3*3*m));

        double denominator = logOdds1 + logOdds2 + logOdds3 + logOdds4 + logOdds5;

        expectedProbabilityArray[0] = logOdds1 / denominator;
        expectedProbabilityArray[1] = logOdds2 / denominator;
        expectedProbabilityArray[2] = logOdds3 / denominator;
        expectedProbabilityArray[3] = logOdds4 / denominator;
        expectedProbabilityArray[4] = logOdds5 / denominator;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// TheoryMediator.cpp: DrinkingPlan TheoryMediator::getIntention() {
// TheoryMediator.cpp: 	mediateThoughtPathway();
// TheoryMediator.cpp: 
// TheoryMediator.cpp: 	double logOdds1 = log( BETA_A_1 * mAttutide + BETA_N_1 * mNorm + BETA_PBC_1 * mPbc );
// TheoryMediator.cpp: 	double logOdds2 = log( BETA_A_2 * mAttutide + BETA_N_2 * mNorm + BETA_PBC_2 * mPbc );
// TheoryMediator.cpp: 	double logOdds3 = log( BETA_A_3 * mAttutide + BETA_N_3 * mNorm + BETA_PBC_3 * mPbc );
// TheoryMediator.cpp: 	double logOdds4 = log( BETA_A_4 * mAttutide + BETA_N_4 * mNorm + BETA_PBC_4 * mPbc );
// TheoryMediator.cpp: 	double logOdds5 = log( BETA_A_5 * mAttutide + BETA_N_5 * mNorm + BETA_PBC_5 * mPbc );
// TheoryMediator.cpp: 
// TheoryMediator.cpp: 	double denom = logOdds1 + logOdds2 + logOdds3 + logOdds4 + logOdds5;
// TheoryMediator.cpp: 
// TheoryMediator.cpp: 	double prob1 = logOdds1 / denom;
// TheoryMediator.cpp: 	double prob2 = logOdds2 / denom;
// TheoryMediator.cpp: 	double prob3 = logOdds3 / denom;
// TheoryMediator.cpp: 	double prob4 = logOdds4 / denom;
// TheoryMediator.cpp: 	double prob5 = logOdds5 / denom;
// TheoryMediator.cpp: 
// TheoryMediator.cpp: 	double total = prob1 + prob2 + prob3 + prob4 + prob5;
// TheoryMediator.cpp: 	double rand = repast::Random::instance()->nextDouble() * total; //random a number from 0 -> total
// TheoryMediator.cpp: 
// TheoryMediator.cpp: 	//propotional selection
// TheoryMediator.cpp: 	DrinkingPlan plan;
// TheoryMediator.cpp: 	if (rand < prob1) {
// TheoryMediator.cpp: 		plan.schema = DrinkingSchema::ABSTAIN;
// TheoryMediator.cpp: 		plan.probability = prob1;
// TheoryMediator.cpp: 	} else if (rand < prob1 + prob2) {
// TheoryMediator.cpp: 		plan.schema = DrinkingSchema::LOW;
// TheoryMediator.cpp: 		plan.probability = prob2;
// TheoryMediator.cpp: 	} else if (rand < prob1 + prob2 + prob3) {
// TheoryMediator.cpp: 		plan.schema = DrinkingSchema::MED;
// TheoryMediator.cpp: 		plan.probability = prob3;
// TheoryMediator.cpp: 	} else if (rand < prob1 + prob2 + prob3 + prob4) {
// TheoryMediator.cpp: 		plan.schema = DrinkingSchema::HIGH;
// TheoryMediator.cpp: 		plan.probability = prob4;
// TheoryMediator.cpp: 	} else {
// TheoryMediator.cpp: 		plan.schema = DrinkingSchema::VERY_HIGH;
// TheoryMediator.cpp: 		plan.probability = prob5;
// TheoryMediator.cpp: 	}
// TheoryMediator.cpp: 	return plan;
// TheoryMediator.cpp: }


       
       resultDrinkingPlan = thisClass.getIntention();
      
       resultProbability = resultDrinkingPlan.probability;

       if(std::find(expectedProbabilityArray, expectedProbabilityArray+5, resultProbability) == expectedProbabilityArray+5){
           nUnequal++;
           std::cout << "resultProbability: " << resultProbability << "expected probabilities: " << expectedProbabilityArray[0];
           std::cout << ", " << expectedProbabilityArray[1] << ", " <<  expectedProbabilityArray[2] << ", " << expectedProbabilityArray[3] << ", ";
           std::cout << expectedProbabilityArray[4] << std::endl;
       }
       std::cout << "resultProbability: " << resultProbability << "expected probabilities: " << expectedProbabilityArray[0];
           std::cout << ", " << expectedProbabilityArray[1] << ", " <<  expectedProbabilityArray[2] << ", " << expectedProbabilityArray[3] << ", ";
           std::cout << expectedProbabilityArray[4] << std::endl;
#ifdef TEST_THEORYMEDIATOR_GETINTENTION_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_THEORYMEDIATOR_GETINTENTION
#ifdef TEST_THEORYMEDIATOR_GETINTENTION_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_TheoryMediator_getIntention elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


// definition of main
int main(int argc, char **argv){
    if(argc < 3){
        std::cout <<"Usage: test_TheoryMediator.exe target nTimes\n";
        return(0);
    }

    std::string target = argv[1];
    int nTimes = std::stoi(argv[2]);
    // examples of how to construct the class based on the cpp file
// TheoryMediator::TheoryMediator(std::vector<Theory*> theoryList) {
//
	TheoryMediator thisClass = TheoryMediator();
    if(target == "all" || target == "test_constructor_1"){
        test_TheoryMediator_1(nTimes );
    }

    if(target == "all" || target == "linkAgent"){
        test_TheoryMediator_linkAgent(nTimes, thisClass);
    }

    if(target == "all" || target == "getIntention"){
        test_TheoryMediator_getIntention(nTimes, thisClass);
    }

}
